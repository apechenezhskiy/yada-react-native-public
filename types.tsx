/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */
import { TextInput as DefaultTextInput, ActivityIndicator as Loading, TouchableOpacity } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';

export type RootStackParamList = {
    Root: undefined;
    NotFound: undefined;
    SignOut: undefined;
};

export type AuthStackParamList = {
    Welcome: undefined;
    SignIn: { username?: string };
    SignUp: undefined;
    Confirmation: { username: string };
    ForgetPassword: undefined;
};

export type IdleStackParamList = {
    Root: undefined;
};

export type BottomTabParamList = {
    DictionaryTab: undefined;
    MyWordsTab: undefined;
};

export type DictionaryTabParamList = {
    Dictionary: undefined;
    Word: { id: number };
    Welcome: undefined;
};

export type MyWordsTabParamList = {
    MyWords: undefined;
    Word: { id: number };
    WordsTrainer: undefined;
    Welcome: undefined;
};

export type WordScreenTabParamList = {
    Dictionary: undefined;
    MyWords: undefined;
    Word: { id: number };
};

export type WordTrainerScreenParamList = {
    Words: Array<{ id: number; word: string }>;
    WordsTrainerResult: undefined;
    WordsTrainer: undefined;
};

export type ButtonProps = TouchableOpacity['props'] & {
    onPress: () => void;
    text: string;
};

export type ThemeProps = {
    lightColor?: string;
    darkColor?: string;
    text?: string;
};

export type InputProps = ThemeProps &
    DefaultTextInput['props'] & {
        isValid?: boolean;
        isLeftIcon?: boolean;
        wrapperStyle?: any;
        showClearButton?: boolean;
        clearButtonPressed?: () => void;
        autoCompleteType?: string;
        leftIcon?: string;
        leftIconColor?: string;
        leftIconStyle?: object;
    };

export type MenuProps = ThemeProps & { signOutHandler: () => void };

export type LoadingProps = Loading['props'] & { isVisible?: boolean };

export type SignInScreenProps = StackScreenProps<AuthStackParamList, 'SignIn'> & { googleSignIn: () => void } & { appleSignIn: () => void };

export type IsMyWordIconProps = { hasUserWord: boolean; onPressCallback: () => void; size: number; color?: string };
