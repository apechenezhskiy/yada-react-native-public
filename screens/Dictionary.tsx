import * as React from 'react';
import { StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';

import { StackScreenProps } from '@react-navigation/stack';
import { ScrollView } from 'react-native-gesture-handler';
import { useIsFocused } from '@react-navigation/native';
import { AxiosResponse } from 'axios';

import { Text, View } from '../components/Themed';
import { Loading } from '../components/Loading';
import { IsMyWordIcon } from '../components/IsMyWordIcon';
import { SearchInput } from '../components/SearchInput';
import { FlatList } from '../components/FlatList';
import { FlatListItem } from '../components/FlatListItem';

import { DictionaryTabParamList } from '../types';
import { AppStyles } from '../AppStyles';

import Api from '../services/api';
import { LookupWordOutputEntry, UserSearchHistoryEntry } from '../generated/';
import { AuthContext, AppAuthContextInterface } from '../navigation/AuthContext';

export default function DictionaryScreen({ navigation, route }: StackScreenProps<DictionaryTabParamList, 'Dictionary'>) {
    const [inputWord, setInputWord] = React.useState('');
    const [wordsArray, setWordsArray] = React.useState<LookupWordOutputEntry[]>([]);
    const [historyArray, setHistoryArray] = React.useState<UserSearchHistoryEntry[]>([]);
    const [timer, setTimer] = React.useState<NodeJS.Timeout | null>(null);
    const [isLoading, setIsLoading] = React.useState(true);
    const [isLoaded, setIsLoaded] = React.useState(false);
    const [isSearching, setIsSearching] = React.useState(false);
    const [isSearchFinished, setIsSearchFinished] = React.useState(false);

    const { userToken } = React.useContext<AppAuthContextInterface>(AuthContext) as AppAuthContextInterface;

    const setUserHistorySearches = ({ data }: AxiosResponse<UserSearchHistoryEntry[]>) => {
        setHistoryArray(data || []);
    };

    const isFocused = useIsFocused();

    React.useEffect(() => {
        if (userToken) {
            Api.getUserHistorySearches()
                .then(setUserHistorySearches)
                .finally(() => {
                    setIsLoading(false);
                    setIsLoaded(true);
                });
        } else {
            setIsLoading(false);
            setIsLoaded(true);
            setHistoryArray([]);
        }

        if (wordsArray.length) {
            Api.lookupWord(inputWord)
                .then(setLookupResultWordsArray)
                .finally(() => {
                    setIsSearching(false);
                    setIsSearchFinished(true);
                });
        }
    }, [isFocused, route]);

    const wordOnPressHandler = (item: UserSearchHistoryEntry) => {
        if (userToken) {
            Api.addUserHistorySearches(item.id).then(() => {
                setHistoryArray((oldArray) => {
                    // Update sort for history array.
                    const itemIndex = oldArray.findIndex((x) => x.id === item.id);
                    if (itemIndex !== -1) {
                        oldArray.splice(itemIndex, 1);
                    }
                    return [item, ...oldArray];
                });
            });
        }

        navigation.navigate('Word', { id: item.id });
    };

    const renderItem = (item: UserSearchHistoryEntry) => {
        return (
            <TouchableOpacity onPress={() => wordOnPressHandler(item)} style={[styles.flItem, styles.verticalCenter]}>
                <Item item={item} />
            </TouchableOpacity>
        );
    };

    const updateArrayByIndex = (oldArray, wordId, is_added, userwordId) => {
        const wordIndex = oldArray.findIndex((x: UserSearchHistoryEntry) => x.id === wordId);
        if (wordIndex > -1) {
            oldArray[wordIndex] = { ...oldArray[wordIndex], is_added: is_added, userword_id: is_added ? userwordId : null };
            return [...oldArray];
        }
        return oldArray;
    };

    const updateIsAddedProperty = (wordId: number, is_added: boolean, userwordId?: number) => {
        setHistoryArray((oldArray) => {
            return updateArrayByIndex(oldArray, wordId, is_added, userwordId);
        });

        setWordsArray((oldArray) => {
            return updateArrayByIndex(oldArray, wordId, is_added, userwordId);
        });
    };

    const addUserWord = (wordId: number) => {
        if (userToken) {
            setIsLoading(true);
            Api.addUserWord(wordId, '')
                .then(({ data }) => {
                    const userwwordId = data?.id;
                    updateIsAddedProperty(wordId, true, userwwordId);
                })
                .finally(() => setIsLoading(false));
        }
    };

    const removeUserWord = (wordId: number, userwordId: number) => {
        if (userToken) {
            setIsLoading(true);
            Api.deleteUserWord(userwordId)
                .then(() => {
                    updateIsAddedProperty(wordId, false);
                })
                .finally(() => setIsLoading(false));
        }
    };

    const Item = ({ item }: { item: LookupWordOutputEntry }) => {
        const onPressCallbackHandler = React.useCallback(
            () => (item.is_added ? removeUserWord(item.id, item.userword_id) : addUserWord(item.id)),
            [item]
        );
        return (
            <View style={styles.item}>
                <Text style={styles.item_title}>{item?.word}</Text>
                {userToken && <IsMyWordIcon size={20} hasUserWord={item?.is_added} onPressCallback={onPressCallbackHandler} />}
            </View>
        );
    };

    const setLookupInputWord = (word: string) => {
        setInputWord(word);
    };

    React.useEffect(() => {
        setLookupTimer(inputWord);
    }, [inputWord]);

    const setLookupResultWordsArray = (currentWord: string, data: LookupWordOutputEntry[]) => {
        if (inputWord === currentWord) {
            setWordsArray(data);
        }
    };

    const setLookupTimer = (word: string) => {
        if (timer) {
            clearTimeout(timer);
        }
        const timerId: ReturnType<typeof setTimeout> = setTimeout(() => {
            setIsSearchFinished(false);
            if (word !== '') {
                setIsSearching(true);
                Api.lookupWord(word)
                    .then(({ data }) => setLookupResultWordsArray(word, data))
                    .finally(() => {
                        setIsSearching(false);
                        setIsSearchFinished(true);
                    });
            } else {
                setWordsArray([]);
            }
        }, 500);
        setTimer(timerId);
    };

    const clearInput = React.useCallback(() => {
        setIsSearchFinished(false);
        setInputWord('');
        setWordsArray([]);
    }, []);

    if (!isLoaded) {
        return <Loading isVisible={true} />;
    }

    const signIn = () => {
        navigation.navigate('Welcome');
    };

    return (
        <View style={styles.container}>
            <SearchInput inputWord={inputWord} setLookupInputWord={setLookupInputWord} clearInput={clearInput}></SearchInput>

            {isSearching ? (
                <ActivityIndicator style={styles.verticalCenter} size={'large'} color={AppStyles.color.primary} />
            ) : wordsArray.length === 0 && historyArray.length === 0 ? (
                <>
                    <Text style={[styles.verticalCenter, styles.infoText]}>{`Yet another dictionary app\nLet’s find some word definitions`}</Text>
                    {!userToken && (
                        <View style={[styles.row, { marginBottom: 20 }]}>
                            <Text style={[styles.letSignInText]}>{`To get access to all Yada's features `}</Text>
                            <Text onPress={signIn} style={styles.letSignInButton}>
                                {'Sign In'}
                            </Text>
                        </View>
                    )}
                </>
            ) : (
                <ScrollView style={styles.main} contentContainerStyle={styles.scrollviewContent}>
                    {wordsArray.length > 0 && (
                        <FlatList>
                            {wordsArray?.map((word: UserSearchHistoryEntry, i) => {
                                return (
                                    <FlatListItem key={word.id} isLastItem={i === wordsArray.length - 1}>
                                        {renderItem(word)}
                                    </FlatListItem>
                                );
                            })}
                        </FlatList>
                    )}
                    {wordsArray.length === 0 &&
                        historyArray.length > 0 &&
                        (!isSearchFinished ? (
                            <FlatList headerText={'Recent'}>
                                {historyArray?.map((word: UserSearchHistoryEntry, i) => {
                                    return (
                                        <FlatListItem key={word.id} isLastItem={i === historyArray.length - 1}>
                                            {renderItem(word)}
                                        </FlatListItem>
                                    );
                                })}
                            </FlatList>
                        ) : (
                            <Text style={[styles.verticalCenter, styles.infoText]}>{'No Results Found'}</Text>
                        ))}
                </ScrollView>
            )}
            {isLoading && <Loading isVisible={true} />}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    main: {
        paddingHorizontal: 10,
        marginVertical: 0,
        width: '100%'
    },
    scrollviewContent: {
        alignItems: 'center'
    },
    column: {
        flexDirection: 'column',
        flex: 1
    },
    row: {
        flexDirection: 'row'
    },
    item_title: {
        fontSize: 16,
        color: '#1E3163'
    },
    fl: {
        width: '100%'
    },
    fl_header: {
        alignItems: 'center'
    },
    item: {
        padding: 13,
        marginVertical: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: AppStyles.color.white
    },
    linkText: {
        fontSize: 14,
        color: '#2e78b7'
    },
    buttonBlock: {
        marginTop: 1
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
        backgroundColor: AppStyles.color.background,
        marginLeft: '5%'
    },
    link: {
        marginTop: -2,
        paddingVertical: 0
    },
    flItem: {
        width: '100%',
        alignSelf: 'center'
    },
    verticalCenter: {
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    infoText: {
        fontSize: 20,
        color: AppStyles.color.primary,
        fontWeight: 'bold',
        textAlign: 'center',
        lineHeight: 50
    },
    wordsListWrapper: {
        alignItems: 'center',
        backgroundColor: AppStyles.color.white,
        borderRadius: 8,
        boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.04), 0px 0px 2px rgba(0, 0, 0, 0.06), 0px 0px 1px rgba(0, 0, 0, 0.04)',
        elevation: 8,
        flexDirection: 'column',
        justifyContent: 'space-between',
        paddingLeft: 16,
        paddingRight: 14,
        marginTop: 15,
        marginBottom: 6
    },
    letSignInText: {
        fontSize: 16,
        color: AppStyles.color.text,
        textAlign: 'center',
        marginBottom: 40
    },
    letSignInButton: {
        fontSize: 16,
        textAlign: 'center',
        color: AppStyles.color.primary
    }
});
