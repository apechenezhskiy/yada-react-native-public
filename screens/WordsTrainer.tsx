/* eslint-disable react/jsx-no-bind */
import * as React from 'react';
import { StyleSheet, Dimensions, Animated, PanResponder, ScaledSize, TouchableWithoutFeedback } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';
import { AxiosResponse } from 'axios';
import { Text, View } from '../components/Themed';
import { Loading } from '../components/Loading';
import { WordHeader } from '../components/WordHeader';
import { MeaningWrapper } from '../components/MeaningWrapper';

import Api from '../services/api';
import { AppStyles } from '../AppStyles';
import { WonderSession, WordListEntry, WonderWord } from '../generated/';

export default function WordsTrainerScreen({ navigation }) {
    const AnimatedTouchableWithoutFeedback = Animated.createAnimatedComponent(TouchableWithoutFeedback);
    const [wordsIndex, setWordsIndex] = React.useState(0);
    const [words, setWords] = React.useState<WonderWord[]>([]);
    const [isLoaded, setIsLoaded] = React.useState(false);
    const [isLoading, setIsLoading] = React.useState(true);
    const position = React.useRef(new Animated.ValueXY()).current;
    const flipAnimation = React.useRef(new Animated.Value(0)).current;

    let flipRotation = 0;
    flipAnimation.addListener(({ value }) => {
        flipRotation = value;
    });

    const theme = useTheme();
    const win = Dimensions.get('window');
    const styles = React.useMemo(() => createStyles(theme, win), [theme, win]);

    const panResponder = React.useMemo(
        () =>
            PanResponder.create({
                onStartShouldSetPanResponder: (evt, gestureState) => true,
                onPanResponderMove: (evt, gestureState) => {
                    position.setValue({ x: gestureState.dx, y: gestureState.dy });
                },
                onPanResponderRelease: (evt, gestureState) => {
                    if (gestureState.dx > 120) {
                        knowWord();
                    } else if (gestureState.dx < -120) {
                        noIdeaWord();
                    } else {
                        Animated.spring(position, {
                            toValue: { x: 0, y: 0 },
                            friction: 4,
                            useNativeDriver: true
                        }).start();
                    }
                },
                onMoveShouldSetPanResponder: (evt, gestureState) => {
                    return Math.abs(gestureState.dx) >= 1 || Math.abs(gestureState.dy) >= 1;
                }
            }),
        [wordsIndex, words]
    );

    React.useEffect(() => {
        position.setValue({ x: 0, y: 0 });
    }, [wordsIndex]);

    // 1. load words&shuffle&get first N of them
    // 2. screen - word in center, 2 buttons - remember/don't remember
    const noIdeaWord = () => {
        const word = words[wordsIndex];
        Api.saveUserAnswer({ userword_id: word.userword_id, answer: -1 });
        showNextWord();
    };

    const knowWord = () => {
        const word = words[wordsIndex];
        Api.saveUserAnswer({ userword_id: word.userword_id, answer: 1 });
        showNextWord();
    };

    const flipCardNoAnimation = () => {
        if (words[wordsIndex].etymologies?.length && words[wordsIndex].etymologies[0].meanings.length) {
            flipAnimation.setValue(flipRotation >= 90 ? 0 : 180);
        }
    };

    const showNextWord = () => {
        if (isArrayFinished()) {
            navigation.replace('WordsTrainerResult');
        } else {
            if (flipRotation >= 90) {
                flipCardNoAnimation();
            }
            setWordsIndex(wordsIndex + 1);
        }
    };

    const isArrayFinished = () => {
        return wordsIndex + 1 >= words.length;
    };

    React.useEffect(() => {
        async function getWords() {
            Api.createWonderSession()
                .then(({ data }: AxiosResponse<WonderSession>) => {
                    setWords(data.words);
                })
                .finally(() => {
                    setIsLoaded(true);
                });
        }
        getWords();
    }, []);

    if (!isLoaded) {
        return <Loading isVisible={true} />;
    }

    const onWordHeaderLoading = (isLoading: boolean) => {
        setIsLoading(isLoading);
    };

    const showWordCard = (id: number) => {
        navigation.navigate('Word', { id: id });
    };

    const rotateAndTranslate = {
        transform: [
            {
                rotate: position.x.interpolate({
                    inputRange: [-win.width / 2, 0, win.width / 2],
                    outputRange: ['-10deg', '0deg', '10deg'],
                    extrapolate: 'clamp'
                })
            },
            ...position.getTranslateTransform()
        ]
    };

    const likeOpacity = position.x.interpolate({
        inputRange: [-win.width / 2, 0, win.width / 2],
        outputRange: [0, 0, 1],
        extrapolate: 'clamp'
    });

    const nopeOpacity = position.x.interpolate({
        inputRange: [-win.width / 2, 0, win.width / 2],
        outputRange: [1, 0, 0],
        extrapolate: 'clamp'
    });

    const nextCardOpacity = position.x.interpolate({
        inputRange: [-win.width / 2, 0, win.width / 2],
        outputRange: [1, 0, 1],
        extrapolate: 'clamp'
    });

    const nextCardScale = position.x.interpolate({
        inputRange: [-win.width / 2, 0, win.width / 2],
        outputRange: [1, 0.8, 1],
        extrapolate: 'clamp'
    });

    const flipCard = () => {
        if (words[wordsIndex].etymologies?.length && words[wordsIndex].etymologies[0].meanings.length) {
            if (flipRotation >= 90) {
                Animated.spring(flipAnimation, {
                    toValue: 0,
                    friction: 8,
                    tension: 10,
                    useNativeDriver: true
                }).start();
            } else {
                Animated.spring(flipAnimation, {
                    toValue: 180,
                    friction: 8,
                    tension: 10,
                    useNativeDriver: true
                }).start();
            }
        }
    };

    const frontAnimatedStyle = {
        transform: [
            {
                rotateY: flipAnimation.interpolate({
                    inputRange: [0, 180],
                    outputRange: ['0deg', '180deg']
                })
            }
        ]
    };
    const backAnimatedStyle = {
        transform: [
            {
                rotateY: flipAnimation.interpolate({
                    inputRange: [0, 180],
                    outputRange: ['180deg', '360deg']
                })
            }
        ]
    };

    const animatedViewWithFlipRender = (word: WonderWord) => {
        return (
            <AnimatedTouchableWithoutFeedback
                onPress={flipCard}
                style={{ marginTop: 'auto', marginBottom: 'auto', backgroundColor: 'transparent', justifyContent: 'center', alignSelf: 'center' }}
            >
                <View style={{ backgroundColor: 'transparent' }}>
                    {word.etymologies?.length > 0 && word.etymologies[0].meanings.length > 0 && (
                        <Animated.View style={[styles.flipCard, styles.flipCardBack, backAnimatedStyle, { backgroundColor: 'transparent' }]}>
                            <View style={styles.animatedViewCardContainer}>
                                <MeaningWrapper meaning={word.etymologies[0].meanings[0]} />
                            </View>
                        </Animated.View>
                    )}
                    <Animated.View style={[styles.flipCard, frontAnimatedStyle, { backgroundColor: 'transparent' }]}>
                        {animatedViewRender(word)}
                    </Animated.View>
                </View>
            </AnimatedTouchableWithoutFeedback>
        );
    };

    const animatedViewRender = (word: WordListEntry) => {
        return (
            <View style={styles.animatedViewCardContainer}>
                <WordHeader
                    word={word}
                    showAddToFavIcon={false}
                    showWordCard={showWordCard}
                    setIsLoading={onWordHeaderLoading}
                    showRightIcon={true}
                />
            </View>
        );
    };

    const getAnimatedViewSwipeText = ({ opacity, color, text, isLeft }) => {
        return (
            <Animated.View
                style={[{ opacity: opacity }, isLeft ? styles.animatedViewSwipeTextContainerLeft : styles.animatedViewSwipeTextContainerRight]}
            >
                <Text style={[{ borderColor: color, color: color }, styles.animatedViewSwipeText]}>{text}</Text>
            </Animated.View>
        );
    };

    return (
        <View style={styles.container}>
            {words.length > 0 &&
                words
                    .map((word, i) => {
                        if (i === wordsIndex) {
                            return (
                                <Animated.View
                                    {...panResponder.panHandlers}
                                    key={i}
                                    style={{
                                        ...rotateAndTranslate,
                                        padding: 10,
                                        position: 'absolute',
                                        height: win.height - 120,
                                        width: win.width,
                                        alignItems: 'center',
                                        backgroundColor: 'transparent'
                                    }}
                                >
                                    {getAnimatedViewSwipeText({ opacity: likeOpacity, color: AppStyles.color.success, text: 'Know', isLeft: true })}
                                    {getAnimatedViewSwipeText({
                                        opacity: nopeOpacity,
                                        color: AppStyles.color.danger,
                                        text: 'No Idea',
                                        isLeft: false
                                    })}
                                    {animatedViewWithFlipRender(word)}
                                </Animated.View>
                            );
                        } else if (i - 1 === wordsIndex) {
                            return (
                                <Animated.View
                                    key={i}
                                    style={{
                                        opacity: i - 1 === wordsIndex ? nextCardOpacity : 0,
                                        transform: [{ scale: nextCardScale }],
                                        height: win.height - 120,
                                        width: win.width,
                                        padding: 10,
                                        position: 'absolute'
                                    }}
                                >
                                    {animatedViewRender(word)}
                                </Animated.View>
                            );
                        } else {
                            return null;
                        }
                    })
                    .reverse()}

            {isLoading && <Loading isVisible={true} />}
        </View>
    );
}

const createStyles = (theme: Theme, win: ScaledSize) =>
    StyleSheet.create({
        container: {
            flex: 1,
            alignItems: 'center',
            alignSelf: 'center'
        },
        animatedViewCardContainer: {
            flex: 1,
            height: null,
            width: null,
            borderRadius: 20,
            justifyContent: 'center',
            backgroundColor: 'transparent'
        },
        animatedViewSwipeTextContainerLeft: {
            transform: [{ rotate: '-30deg' }],
            position: 'absolute',
            top: 50,
            left: 40,
            zIndex: 1000
        },
        animatedViewSwipeTextContainerRight: {
            transform: [{ rotate: '30deg' }],
            position: 'absolute',
            top: 50,
            right: 40,
            zIndex: 1000
        },
        animatedViewSwipeText: {
            borderWidth: 1,
            fontSize: 32,
            fontWeight: '800',
            padding: 10
        },
        flipCard: {
            borderRadius: 10,
            alignItems: 'center',
            justifyContent: 'center',
            backfaceVisibility: 'hidden',
            shadowOpacity: 0.4,
            shadowRadius: 10,
            shadowOffset: { width: 4, height: 4 },
            shadowColor: AppStyles.color.lightAccent,
            elevation: 2,
            alignSelf: 'center'
        },
        flipCardBack: {
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
        },
        cardBackText: {
            color: AppStyles.color.text,
            textAlign: 'center',
            paddingHorizontal: 10,
            paddingVertical: 10
        },
        wordMeaningBack: {
            fontSize: 18,
            textAlign: 'justify'
        }
    });
