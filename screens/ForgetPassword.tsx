import { StackScreenProps } from '@react-navigation/stack';
import * as React from 'react';
import { Auth } from 'aws-amplify';
import { StyleSheet, View, KeyboardAvoidingView } from 'react-native';

import { AuthStackParamList } from '../types';
import { AppStyles } from '../AppStyles';
import { emailValidator } from '../helpers/emailValidator';
import { passwordValidator } from '../helpers/passwordValidator';
import { confirmationCodeValidator } from '../helpers/confirmationCodeValidator';
import { Button } from '../components/Button';
import { Input } from '../components/Input';
import { Text, ErrorText } from '../components/Themed';
import { showToaster } from '../helpers/showToaster';

export default function ForgetPassword({ navigation }: StackScreenProps<AuthStackParamList, 'ForgetPassword'>) {
    const [email, setEmail] = React.useState('');
    const [emailError, setEmailError] = React.useState('');
    const [editableInput, setEditableInput] = React.useState(true);
    const [confirmationStep, setConfirmationStep] = React.useState(false);
    const [code, setCode] = React.useState('');
    const [codeError, setCodeError] = React.useState('');
    const [newPassword, setNewPassword] = React.useState('');
    const [newPasswordError, setNewPasswordError] = React.useState('');

    const getConfirmationCode = React.useCallback(async () => {
        const emailErrorRes = emailValidator(email);
        setEmailError(emailErrorRes);
        if (emailErrorRes) {
            return;
        }
        Auth.forgotPassword(email)
            .then(() => {
                setEditableInput(false);
                setConfirmationStep(true);
            })
            .catch((err) => {
                if (err.message) {
                    showToaster(err.message);
                }
            });
    }, [email]);

    const postNewPassword = React.useCallback(async () => {
        const passwordError = passwordValidator(newPassword);
        setNewPasswordError(passwordError);
        if (codeError != '' || passwordError != '') {
            return;
        }
        Auth.forgotPasswordSubmit(email, code, newPassword)
            .then(() => {
                navigation.navigate('SignIn', { username: email });
            })
            .catch((err) => {
                if (err.message) {
                    showToaster(err.message);
                }
            });
    }, [newPassword, email, code]);

    const inputEmailBlurHandler = React.useCallback(() => {
        setEmailError(emailValidator(email));
    }, [email]);

    const inputCodeBlurHandler = React.useCallback(() => {
        setCodeError(confirmationCodeValidator(code));
    }, [code]);

    const inputPasswordBlurHandler = React.useCallback(() => {
        setNewPasswordError(passwordValidator(newPassword));
    }, [newPassword]);

    return (
        <KeyboardAvoidingView style={styles.baseView} behavior={'position'}>
            <View style={{ flex: !confirmationStep ? 0.4 : 0.2, justifyContent: 'center' }}>
                <Text style={styles.title}>Yada</Text>
            </View>
            <View style={[styles.loginCardView, { flex: !confirmationStep ? 0.6 : 0.8 }]}>
                <View style={styles.itemSeparator}></View>
                <Text style={styles.loginCardLabel}>Reset Password</Text>
                <Input
                    style={styles.searchInput}
                    wrapperStyle={styles.searchInputContainer}
                    isLeftIcon={true}
                    leftIcon={'mail'}
                    leftIconStyle={{ backgroundColor: '#F9F9F9' }}
                    leftIconColor={AppStyles.color.primary}
                    placeholder={'Email'}
                    value={email}
                    onChangeText={setEmail}
                    onBlur={inputEmailBlurHandler}
                    isValid={emailError !== ''}
                    editable={editableInput}
                    placeholderTextColor={AppStyles.color.grey}
                    autoCapitalize={'none'}
                    autoCompleteType={'email'}
                    textContentType={'emailAddress'}
                    keyboardType={'email-address'}
                />
                <ErrorText text={emailError} />
                <Button onPress={getConfirmationCode} text={'Reset password via email'} />
                {confirmationStep && (
                    <>
                        <Text style={styles.subtitle}>Check your email for the confirmation code.</Text>
                        <Input
                            style={styles.searchInput}
                            wrapperStyle={styles.searchInputContainer}
                            isLeftIcon={true}
                            leftIcon={'code-working'}
                            leftIconStyle={{ backgroundColor: '#F9F9F9' }}
                            leftIconColor={AppStyles.color.primary}
                            placeholder={'Confirmation code'}
                            value={code}
                            onBlur={inputCodeBlurHandler}
                            onChangeText={setCode}
                            placeholderTextColor={AppStyles.color.grey}
                        />
                        <ErrorText text={codeError} />
                        <Input
                            style={styles.searchInput}
                            wrapperStyle={styles.searchInputContainer}
                            isLeftIcon={true}
                            leftIcon={'lock-closed'}
                            leftIconStyle={{ backgroundColor: '#F9F9F9' }}
                            leftIconColor={AppStyles.color.primary}
                            placeholder={'New Password'}
                            value={newPassword}
                            onChangeText={setNewPassword}
                            onBlur={inputPasswordBlurHandler}
                            isValid={newPasswordError !== ''}
                            secureTextEntry={true}
                            autoCompleteType={'password'}
                            placeholderTextColor={AppStyles.color.grey}
                        />
                        <ErrorText text={newPasswordError} />
                        <Button onPress={postNewPassword} text={'Submit new password'} />
                    </>
                )}
            </View>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        marginTop: 4
    },
    title: {
        color: AppStyles.color.white,
        fontWeight: 'bold',
        fontSize: 38,
        textAlign: 'center'
    },
    baseView: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        backgroundColor: AppStyles.color.primary
    },
    loginCardView: {
        justifyContent: 'space-around',
        backgroundColor: AppStyles.color.white,
        boxShadow: '0px -25px 32px rgba(82, 130, 255, 0.09)',
        borderRadius: 34,
        alignItems: 'center'
    },
    loginCardLabel: {
        fontWeight: '500',
        fontSize: 28,
        lineHeight: 42,
        textAlign: 'center'
    },
    itemSeparator: {
        height: 6,
        width: '20%',
        backgroundColor: '#EDEDED',
        alignSelf: 'center',
        borderRadius: 16
    },
    forgotPassword: {
        fontSize: 14,
        lineHeight: 21,
        marginRight: 10
    },
    searchInput: {
        fontSize: 18,
        width: '80%',
        height: 68,
        paddingLeft: 28
    },
    searchInputContainer: {
        backgroundColor: '#F9F9F9',
        borderWidth: 0,
        borderRadius: 8,
        width: '85%'
    },
    subtitle: {
        fontSize: 14,
        lineHeight: 22,
        textAlign: 'center',
        color: '#929EBA',
        width: '80%'
    }
});
