import * as React from 'react';
import { StyleSheet } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import { View } from '../components/Themed';
import { Button } from '../components/Button';
import { TextButton } from '../components/TextButton';
import { Text } from '../components/Themed';

export default function WordsTrainerResult({ navigation }: StackScreenProps<WordTrainerScreenParamList, 'WordsTrainerResult'>) {
    const onContinuePress = React.useCallback(() => {
        navigation.replace('WordsTrainer');
    }, []);

    const onBackToWordsPress = React.useCallback(() => {
        navigation.goBack();
    }, []);

    return (
        <View style={styles.container}>
            <View style={styles.resultTextWrapper}>
                <Text style={styles.resultText}>Great Work! You have learned some new words. Stay on track. Let's practice more?</Text>
            </View>
            <View style={styles.buttonsWrapper}>
                <Button style={styles.continueButton} text={'Continue'} onPress={onContinuePress} />
                <TextButton onPress={onBackToWordsPress} text={'Back to Words'} />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        display: 'flex'
    },
    continueButton: {
        position: 'absolute',
        bottom: 0,
        left: 0
    },
    buttonsWrapper: {
        width: '90%',
        alignItems: 'center'
    },
    resultText: {
        fontWeight: '500',
        fontSize: 24,
        textAlign: 'center'
    },
    resultTextWrapper: {
        width: '90%',
        alignItems: 'center'
    }
});
