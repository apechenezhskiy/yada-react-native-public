import * as React from 'react';
import { StyleSheet } from 'react-native';
import { Auth } from 'aws-amplify';
import { CognitoHostedUIIdentityProvider } from '@aws-amplify/auth/lib/types';
import { SocialButton } from '../components/SocialButtons/SocialButton';

import { SignInScreenProps } from '../types';
import { AppStyles } from '../AppStyles';
import { TextButton } from '../components/TextButton';
import { Text, View, BaseView } from '../components/Themed';

function WelcomeScreen({ navigation }: SignInScreenProps) {
    const styles = React.useMemo(() => createStyles(), []);

    const signInGoogle = React.useCallback(() => {
        Auth.federatedSignIn({ provider: CognitoHostedUIIdentityProvider.Google });
    }, []);

    const signInApple = React.useCallback(() => {
        Auth.federatedSignIn({ provider: CognitoHostedUIIdentityProvider.Apple });
    }, []);

    const goToSignIn = React.useCallback(() => {
        navigation.navigate('SignIn');
    }, []);

    const goToSignUp = React.useCallback(() => {
        navigation.navigate('SignUp');
    }, []);

    return (
        <BaseView>
            <View style={{ display: 'flex', flexDirection: 'column' }}>
                <View
                    style={{
                        flexGrow: 4,
                        justifyContent: 'flex-end'
                    }}
                >
                    <Text style={styles.yadaTitle}>{'Yada'}</Text>
                </View>
                <View
                    style={{
                        flexGrow: 2,
                        justifyContent: 'center'
                    }}
                >
                    <Text style={styles.welcomeTitle}>Welcome</Text>
                    <Text style={styles.signinTitle}>Sign in below and let’s make some magic</Text>
                    <View>
                        <SocialButton
                            buttonText={'Sign in with Google'}
                            onPress={signInGoogle}
                            source={require('../components/SocialButtons/google.png')}
                        />
                        <SocialButton
                            buttonText={'Sign in with Apple'}
                            onPress={signInApple}
                            source={require('../components/SocialButtons/apple.png')}
                        />
                        <SocialButton
                            buttonText={'Sign in with Email'}
                            onPress={goToSignIn}
                            source={require('../components/SocialButtons/mail.png')}
                        />
                    </View>
                </View>

                <View
                    style={{
                        flexGrow: 2,
                        justifyContent: 'center'
                    }}
                >
                    <Text style={styles.text}>Don’t have an account?</Text>
                    <TextButton onPress={goToSignUp} text={'Sign Up Here'} />
                </View>
            </View>
        </BaseView>
    );
}

const createStyles = () =>
    StyleSheet.create({
        yadaTitle: {
            color: AppStyles.color.primary,
            fontSize: 38,
            fontWeight: 'bold',
            lineHeight: 42,
            textAlign: 'center'
        },
        welcomeTitle: {
            color: AppStyles.color.text,
            fontSize: 28,
            fontWeight: '500',
            lineHeight: 42,
            textAlign: 'center'
        },
        signinTitle: {
            color: AppStyles.color.text,
            fontSize: 14,
            fontWeight: '400',
            lineHeight: 42,
            textAlign: 'center'
        },
        text: {
            color: AppStyles.color.text,
            fontSize: 14,
            fontWeight: '400',
            lineHeight: 21,
            textAlign: 'center'
        }
    });

export default WelcomeScreen;
