import * as React from 'react';
import { StackScreenProps } from '@react-navigation/stack';
import { StyleSheet, View, KeyboardAvoidingView } from 'react-native';
import { Auth } from 'aws-amplify';

import { AuthStackParamList } from '../types';
import { AppStyles } from '../AppStyles';
import { emailValidator } from '../helpers/emailValidator';
import { passwordValidator } from '../helpers/passwordValidator';
import { Text, ErrorText } from '../components/Themed';
import { Button } from '../components/Button';
import { Input } from '../components/Input';
import { Loading } from '../components/Loading';
import { showToaster } from '../helpers/showToaster';
import AmplitudeUtils from '../services/amplitudeUtils';

export default function SignUpScreen({ navigation }: StackScreenProps<AuthStackParamList, 'SignUp'>) {
    const [email, setEmail] = React.useState('');
    const [emailError, setEmailError] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [passwordError, setPasswordError] = React.useState('');
    const [isLoading, setIsLoading] = React.useState(false);

    const signUp = React.useCallback(async () => {
        const emailErrorRes = emailValidator(email);
        const passwordErrorRes = passwordValidator(password);
        setEmailError(emailErrorRes);
        setPasswordError(passwordErrorRes);
        if (emailErrorRes != '' || passwordErrorRes != '') {
            return;
        }
        setIsLoading(true);
        Auth.signUp({
            username: email,
            password: password,
            attributes: {
                email: email
            }
        })
            .then(() => {
                AmplitudeUtils.logEventAmpl('registered');
                navigation.navigate('SignIn', { username: email });
            })
            .catch((err) => {
                if (err.code === 'UserNotConfirmedException') {
                    showToaster('Account not verified yet');
                } else if (err.code === 'PasswordResetRequiredException') {
                    showToaster('Existing user found. Please reset your password');
                } else if (err.code === 'NotAuthorizedException') {
                    showToaster('Forgot Password?');
                } else if (err.code === 'UserNotFoundException') {
                    showToaster('User does not exist!');
                } else {
                    showToaster('Error' + err.code);
                }
            })
            .finally(() => setIsLoading(false));
    }, [email, password]);

    const inputEmailBlurHandler = React.useCallback(() => {
        setEmailError(emailValidator(email));
    }, [email]);

    const inputPasswordBlurHandler = React.useCallback(() => {
        setPasswordError(passwordValidator(password));
    }, [password]);

    return (
        <KeyboardAvoidingView style={styles.baseView} behavior={'position'}>
            <View style={{ flex: 0.4, justifyContent: 'center' }}>
                <Text style={styles.title}>Yada</Text>
            </View>
            <View style={styles.loginCardView}>
                <View style={styles.itemSeparator}></View>
                <Text style={styles.loginCardLabel}>Create an account</Text>
                <Text style={styles.subtitle}>Welcome to Yada! We’re so glad you’re here. Fill out the info below to get started.</Text>
                <Input
                    style={styles.searchInput}
                    wrapperStyle={styles.searchInputContainer}
                    isLeftIcon={true}
                    leftIcon={'mail'}
                    leftIconStyle={{ backgroundColor: '#F9F9F9' }}
                    leftIconColor={AppStyles.color.primary}
                    onBlur={inputEmailBlurHandler}
                    isValid={emailError !== ''}
                    placeholder={'Email'}
                    value={email}
                    onChangeText={setEmail}
                    placeholderTextColor={AppStyles.color.grey}
                    autoCapitalize={'none'}
                    autoCompleteType={'email'}
                    textContentType={'emailAddress'}
                    keyboardType={'email-address'}
                />
                <ErrorText text={emailError} />
                <Input
                    style={styles.searchInput}
                    wrapperStyle={styles.searchInputContainer}
                    isLeftIcon={true}
                    leftIcon={'lock-closed'}
                    leftIconStyle={{ backgroundColor: '#F9F9F9' }}
                    leftIconColor={AppStyles.color.primary}
                    placeholder={'Password'}
                    value={password}
                    onChangeText={setPassword}
                    onBlur={inputPasswordBlurHandler}
                    isValid={passwordError !== ''}
                    secureTextEntry={true}
                    placeholderTextColor={AppStyles.color.grey}
                />
                <ErrorText text={passwordError} />
                <Button onPress={signUp} text={'Create an account'} style={{ marginBottom: 20 }} />
            </View>
            {isLoading && <Loading isVisible={true} />}
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        marginTop: 4
    },
    title: {
        color: AppStyles.color.white,
        fontWeight: 'bold',
        fontSize: 38,
        textAlign: 'center'
    },
    baseView: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        backgroundColor: AppStyles.color.primary
    },
    loginCardView: {
        flex: 0.6,
        justifyContent: 'space-around',
        backgroundColor: AppStyles.color.white,
        boxShadow: '0px -25px 32px rgba(82, 130, 255, 0.09)',
        borderRadius: 34,
        alignItems: 'center'
    },
    loginCardLabel: {
        fontWeight: '500',
        fontSize: 28,
        lineHeight: 42,
        textAlign: 'center'
    },
    itemSeparator: {
        height: 6,
        width: '20%',
        backgroundColor: '#EDEDED',
        alignSelf: 'center',
        borderRadius: 16
    },
    forgotPassword: {
        fontSize: 14,
        lineHeight: 21,
        marginRight: 10
    },
    searchInput: {
        fontSize: 18,
        width: '80%',
        height: 68,
        paddingLeft: 28
    },
    searchInputContainer: {
        backgroundColor: '#F9F9F9',
        borderWidth: 0,
        borderRadius: 8,
        width: '85%'
    },
    subtitle: {
        fontSize: 14,
        lineHeight: 22,
        textAlign: 'center',
        color: '#929EBA',
        width: '80%',
        maxWidth: '80%'
    }
});
