import * as React from 'react';
import { StyleSheet } from 'react-native';
import { Text, View } from '../components/Themed';
import { AppStyles } from '../AppStyles';

export default function Splash() {
    return (
        <View style={styles.container}>
            <Text style={styles.centerText}>Yada</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: AppStyles.color.primary,
        padding: 8
    },
    centerText: {
        fontSize: 78,
        color: AppStyles.color.white
    }
});
