import { StackScreenProps } from '@react-navigation/stack';
import * as React from 'react';
import { Auth } from 'aws-amplify';

import { AuthStackParamList } from '../types';
import { AppStyles } from '../AppStyles';
import { confirmationCodeValidator } from '../helpers/confirmationCodeValidator';
import { Button } from '../components/Button';
import { Input } from '../components/Input';
import { Text, BaseView } from '../components/Themed';
import { showToaster } from '../helpers/showToaster';

export default function ConfirmationScreen({ route, navigation }: StackScreenProps<AuthStackParamList, 'Confirmation'>) {
    const [authCode, setAuthCode] = React.useState('');
    const { username } = route.params;

    const confirmSignUp = async () => {
        const error = confirmationCodeValidator(authCode);
        showToaster(error);
        if (error != '') {
            await Auth.confirmSignUp(username, authCode)
                .then(() => {
                    navigation.navigate('SignIn');
                })
                .catch((err) => {
                    if (!err.message) {
                        showToaster('Something went wrong, please contact support!');
                    } else {
                        showToaster(err.message);
                    }
                });
        }
    };

    const inputCodeBlurHandler = () => {
        showToaster(confirmationCodeValidator(authCode));
    };

    return (
        <BaseView>
            <Text>Check your email for the confirmation code.</Text>
            <Input
                placeholder={'123456'}
                value={authCode}
                onBlur={inputCodeBlurHandler}
                onChangeText={setAuthCode}
                placeholderTextColor={AppStyles.color.grey}
            />
            <Button onPress={confirmSignUp} text={'Confirm Sign Up'} />
        </BaseView>
    );
}
