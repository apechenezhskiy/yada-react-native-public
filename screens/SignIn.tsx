import * as React from 'react';
import { StyleSheet, View, KeyboardAvoidingView } from 'react-native';
import { Auth } from 'aws-amplify';

import { SignInScreenProps } from '../types';
import { AppStyles } from '../AppStyles';
import { emailValidator } from '../helpers/emailValidator';
import { passwordValidator } from '../helpers/passwordValidator';
import { Button } from '../components/Button';
import { TextButton } from '../components/TextButton';
import { Input } from '../components/Input';
import { Text, ErrorText } from '../components/Themed';
import { Loading } from '../components/Loading';
import { showToaster } from '../helpers/showToaster';

function SignInScreen({ route, navigation }: SignInScreenProps) {
    const [email, setEmail] = React.useState('');
    const [emailError, setEmailError] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [passwordError, setPasswordError] = React.useState('');
    const [isLoading, setIsLoading] = React.useState(false);
    const [isShowConfirmationError, setIsShowConfirmationError] = React.useState(false);
    const styles = React.useMemo(() => createStyles(), []);

    React.useEffect(() => {
        if (route?.params?.username) {
            setEmail(route?.params?.username || '');
            setEmailError('');
            setPassword('');
            setPasswordError('');
        }
    }, [route]);

    const signIn = React.useCallback(async () => {
        const emailError = emailValidator(email);
        const passwordError = passwordValidator(password);
        setEmailError(emailError);
        setPasswordError(passwordError);
        if (emailError != '' || passwordError != '') {
            return;
        }

        setIsLoading(true);
        Auth.signIn(email, password)
            .catch((err) => {
                if (!err.message) {
                    console.log('Error when signing in: ', err);
                } else {
                    if (err.code === 'UserNotConfirmedException') {
                        setIsShowConfirmationError(true);
                        showToaster('User does not confirmed');
                    } else if (err.code === 'PasswordResetRequiredException') {
                        showToaster('Existing user found. Please reset your password');
                    } else if (err.code === 'NotAuthorizedException') {
                        showToaster('Forgot Password?');
                    } else if (err.code === 'UserNotFoundException') {
                        showToaster('User does not exist!');
                    } else {
                        showToaster(err.code);
                    }
                }
            })
            .finally(() => setIsLoading(false));
    }, [email, password]);

    const goToForgetPassword = React.useCallback(() => {
        navigation.navigate('ForgetPassword');
    }, []);

    const inputEmailBlurHandler = React.useCallback(() => {
        setEmailError(emailValidator(email));
    }, [email]);

    const inputPasswordBlurHandler = React.useCallback(() => {
        setPasswordError(passwordValidator(password));
    }, [password]);

    const sendVerifyLink = React.useCallback(() => {
        setIsLoading(true);
        Auth.resendSignUp(email)
            .then(() => {
                showToaster('Sent');
            })
            .finally(() => setIsLoading(false));
    }, [email]);

    return (
        <KeyboardAvoidingView style={styles.baseView} behavior={'position'}>
            <View style={{ flex: 0.4, justifyContent: 'center' }}>
                <Text style={styles.title}>Yada</Text>
            </View>
            <View style={styles.loginCardView}>
                <View style={styles.itemSeparator}></View>
                <Text style={styles.loginCardLabel}>Login with Email</Text>
                <Input
                    style={styles.searchInput}
                    wrapperStyle={styles.searchInputContainer}
                    isLeftIcon={true}
                    leftIcon={'mail'}
                    leftIconStyle={{ backgroundColor: '#F9F9F9' }}
                    leftIconColor={AppStyles.color.primary}
                    placeholder={'Email'}
                    value={email}
                    onChangeText={setEmail}
                    onBlur={inputEmailBlurHandler}
                    isValid={emailError !== ''}
                    placeholderTextColor={AppStyles.color.grey}
                    autoCapitalize={'none'}
                    autoCompleteType={'email'}
                    textContentType={'emailAddress'}
                    keyboardType={'email-address'}
                />
                <ErrorText text={emailError} />
                <Input
                    style={styles.searchInput}
                    wrapperStyle={styles.searchInputContainer}
                    isLeftIcon={true}
                    leftIcon={'lock-closed'}
                    leftIconStyle={{ backgroundColor: '#F9F9F9' }}
                    leftIconColor={AppStyles.color.primary}
                    placeholder={'Password'}
                    value={password}
                    onChangeText={setPassword}
                    onBlur={inputPasswordBlurHandler}
                    isValid={passwordError !== ''}
                    secureTextEntry={true}
                    placeholderTextColor={AppStyles.color.grey}
                />
                <ErrorText text={passwordError} />
                <Button onPress={signIn} text={'Sign In'} />
                {isShowConfirmationError && (
                    <>
                        <ErrorText text={'To activate account, please click the link in your email'} />
                        <TextButton onPress={sendVerifyLink} text={'Send link again'} />
                    </>
                )}
                <View style={[styles.row, { marginBottom: 20 }]}>
                    <Text style={styles.forgotPassword}>Forgot your password?</Text>
                    <TextButton onPress={goToForgetPassword} text={'Reset here'} />
                </View>
            </View>

            {isLoading && <Loading isVisible={true} />}
        </KeyboardAvoidingView>
    );
}

const createStyles = () =>
    StyleSheet.create({
        row: {
            flexDirection: 'row',
            marginTop: 4
        },
        title: {
            color: AppStyles.color.white,
            fontWeight: 'bold',
            fontSize: 38,
            textAlign: 'center'
        },
        baseView: {
            flex: 1,
            width: '100%',
            height: '100%',
            alignItems: 'center',
            backgroundColor: AppStyles.color.primary
        },
        loginCardView: {
            flex: 0.6,
            justifyContent: 'space-around',
            backgroundColor: AppStyles.color.white,
            boxShadow: '0px -25px 32px rgba(82, 130, 255, 0.09)',
            borderRadius: 34,
            alignItems: 'center'
        },
        loginCardLabel: {
            fontWeight: '500',
            fontSize: 28,
            lineHeight: 42,
            textAlign: 'center'
        },
        itemSeparator: {
            height: 6,
            width: '20%',
            backgroundColor: '#EDEDED',
            alignSelf: 'center',
            borderRadius: 16
        },
        forgotPassword: {
            fontSize: 14,
            lineHeight: 21,
            marginRight: 10
        },
        searchInput: {
            fontSize: 18,
            width: '80%',
            height: 68,
            paddingLeft: 28
        },
        searchInputContainer: {
            backgroundColor: '#F9F9F9',
            borderWidth: 0,
            borderRadius: 8,
            width: '85%'
        }
    });

export default SignInScreen;
