import * as React from 'react';
import { Text, View } from '../components/Themed';
import { AuthContext, AppAuthContextInterface } from '../navigation/AuthContext';

export default function SignOutScreen() {
    const { signOut: signOutCb } = React.useContext<AppAuthContextInterface>(AuthContext) as AppAuthContextInterface;
    signOutCb();

    return (
        <View>
            <Text>Signed Out</Text>
        </View>
    );
}
