import * as React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import { useIsFocused } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';
import { AxiosResponse } from 'axios';
import moment from 'moment';
import { Button } from '../components/Button';
import { Text, View } from '../components/Themed';
import { Loading } from '../components/Loading';
import { SearchInput } from '../components/SearchInput';
import { FlatList } from '../components/FlatList';
import { FlatListItem } from '../components/FlatListItem';
import { ProgressIndicator } from '../components/ProgressIndicator';

import { MyWordsTabParamList } from '../types';
import { AppStyles } from '../AppStyles';

import Api from '../services/api';
import AmplitudeUtils from '../services/amplitudeUtils';
import { WordListEntry } from '../generated/';
import { AuthContext, AppAuthContextInterface } from '../navigation/AuthContext';

export default function MyWordsScreen({ navigation }: StackScreenProps<MyWordsTabParamList, 'MyWords'>) {
    const [inputWord, setInputWord] = React.useState('');
    const [wordsArray, setWordsArray] = React.useState<WordListEntry[]>([]);
    const [wordsArrayWithoutFilter, setWordsArrayWithoutFilter] = React.useState<WordListEntry[]>([]);
    const [isLoaded, setIsLoaded] = React.useState(false);
    const { userToken } = React.useContext<AppAuthContextInterface>(AuthContext) as AppAuthContextInterface;

    const Item = ({ title, date_unix }: { title: string; date_unix: number }) => {
        const date = new Date(date_unix * 1000);

        return (
            <View style={styles.item}>
                <Text style={styles.item_title}>{title}</Text>
                <Text style={styles.item_subtitle}>Added {moment(date).fromNow()}</Text>
            </View>
        );
    };

    const renderItem = (item: WordListEntry) => {
        const goToWord = () => {
            navigation.navigate('Word', { id: item.word_id });
        };
        return (
            <TouchableOpacity onPress={goToWord} style={[styles.flItem, styles.verticalCenter]}>
                <Item title={item.word} date_unix={+item.date_added} />
                <ProgressIndicator progress={item.progress} />
            </TouchableOpacity>
        );
    };

    const isFocused = useIsFocused();

    React.useEffect(() => {
        async function getWords() {
            Api.getUserWords().then(({ data }: AxiosResponse<WordListEntry[]>) => {
                AmplitudeUtils.setUserProperties({ words_count: data.length });
                setWordsArray(data);
                setWordsArrayWithoutFilter(data);
                setIsLoaded(true);
            });
        }

        if (userToken && isFocused) {
            getWords();
        } else {
            setIsLoaded(true);
        }
    }, [isFocused]);

    const onTrainButton = React.useCallback(() => {
        navigation.navigate('WordsTrainer');
    }, []);

    const setLookupInputWord = (word: string) => {
        setInputWord(word);
        if (word !== '') {
            setWordsArray(wordsArray.filter((x) => x.word.toLowerCase().startsWith(word.toLowerCase())));
        } else {
            setWordsArray(wordsArrayWithoutFilter);
        }
    };

    const clearInput = React.useCallback(() => {
        setInputWord('');
        setWordsArray(wordsArrayWithoutFilter);
    }, []);

    if (!isLoaded) {
        return <Loading isVisible={true} />;
    }

    const signIn = () => {
        navigation.navigate('Welcome');
    };

    return (
        <View style={styles.container}>
            <SearchInput inputWord={inputWord} setLookupInputWord={setLookupInputWord} clearInput={clearInput}></SearchInput>
            {wordsArray.length === 0 ? (
                <>
                    {userToken ? (
                        <Text style={[styles.verticalCenter, styles.infoText]}>{`Add some words to train them`}</Text>
                    ) : (
                        <View style={[styles.row, styles.verticalCenter]}>
                            <Text style={[styles.verticalCenter, styles.letSignInText]}>
                                {`To be able to add words and train them `}
                                <Text onPress={signIn} style={styles.letSignInButton}>
                                    {'Sign In'}
                                </Text>
                            </Text>
                        </View>
                    )}
                </>
            ) : (
                <>
                    <ScrollView style={styles.main} contentContainerStyle={styles.scrollviewContent}>
                        <FlatList>
                            {wordsArray?.map((word: WordListEntry, i) => {
                                return (
                                    <FlatListItem key={word.id} isLastItem={i === wordsArray.length - 1}>
                                        {renderItem(word)}
                                    </FlatListItem>
                                );
                            })}
                        </FlatList>
                    </ScrollView>
                    <Button style={styles.btn_trainWords} text={'Train words'} onPress={onTrainButton} />
                </>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '93%'
    },
    verticalCenter: {
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    infoText: {
        fontSize: 20,
        color: AppStyles.color.primary,
        fontWeight: 'bold',
        textAlign: 'center',
        lineHeight: 50
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%'
    },
    fl: {
        width: '100%'
    },
    item: {
        padding: 13,
        marginVertical: 0,
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: AppStyles.color.white
    },
    item_title: {
        fontSize: 16,
        color: '#1E3163'
    },
    item_subtitle: {
        fontSize: 10,
        color: '#34323e'
    },
    btn_trainWords: {
        position: 'absolute',
        bottom: 0
    },
    flItem: {
        width: '100%',
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    main: {
        paddingHorizontal: 10,
        marginVertical: 0,
        width: '100%'
    },
    scrollviewContent: {
        alignItems: 'center'
    },
    progressIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: '1%',
        backgroundColor: AppStyles.color.white
    },
    letSignInText: {
        fontSize: 26,
        color: AppStyles.color.text,
        textAlign: 'center'
    },
    letSignInButton: {
        fontSize: 26,
        color: AppStyles.color.primary
    }
});
