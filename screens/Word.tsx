import * as React from 'react';
import { NativeScrollEvent, NativeSyntheticEvent, StyleSheet } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import { AxiosResponse } from 'axios';
import { View } from '../components/Themed';
import { Loading } from '../components/Loading';
import { WordScreenTabParamList } from '../types';
import { WordHeader } from '../components/WordHeader';
import Api from '../services/api';
import { ScrollView } from 'react-native-gesture-handler';
import { EtymologyWrapper } from '../components/EtymologyWrapper';
import { Word, Etymology } from '../generated/';
import { AppStyles } from '../AppStyles';
import { AuthContext, AppAuthContextInterface } from '../navigation/AuthContext';

export default function WordScreen({ route, navigation }: StackScreenProps<WordScreenTabParamList, 'Word'>) {
    const { id } = route.params;

    const [word, setWord] = React.useState<Word>();
    const [isLoaded, setIsLoaded] = React.useState(false);
    const [isLoading, setIsLoading] = React.useState(true);
    const { userToken } = React.useContext<AppAuthContextInterface>(AuthContext) as AppAuthContextInterface;

    const setWordsValues = async ({ data: word }: AxiosResponse<Word>) => {
        setWord(word);
    };

    React.useEffect(() => {
        async function getWord() {
            Api.getWord(id)
                .then(setWordsValues)
                .then(() => setIsLoaded(true))
                .catch(() => setIsLoaded(false)); // #TODO temporary solution
        }
        getWord();
    }, []);

    const onWordHeaderLoading = React.useCallback((isLoading: boolean) => {
        setIsLoading(isLoading);
    }, []);

    const updateScreenTitleOnScroll = React.useCallback(
        (ev: NativeSyntheticEvent<NativeScrollEvent>) => {
            if (ev.nativeEvent.contentOffset.y > 40) {
                navigation.setOptions({ headerTitle: word?.word, headerTitleStyle: { fontSize: 30 } });
            } else {
                navigation.setOptions({ headerTitle: '' });
            }
        },
        [word]
    );

    const onWordChangedBase = React.useCallback(
        (isAdded: boolean, userwordId?: number) => {
            setWord({ ...word, is_added: isAdded, userword_id: userwordId });
        },
        [word]
    );

    if (!isLoaded) {
        return <Loading isVisible={true} />;
    }

    return (
        <View style={styles.container}>
            <ScrollView style={styles.main} onScroll={updateScreenTitleOnScroll} scrollEventThrottle={50}>
                <View style={styles.mainPadding}>
                    <WordHeader
                        word={word}
                        showAddToFavIcon={true}
                        setIsLoading={onWordHeaderLoading}
                        headerColor={AppStyles.color.text}
                        onWordChanged={onWordChangedBase}
                        showRightIcon={!!userToken}
                    />
                    {word?.etymologies.map((etymology: Etymology) => {
                        return <EtymologyWrapper etymology={etymology} key={etymology.id} />;
                    })}
                </View>
            </ScrollView>
            {isLoading && <Loading isVisible={true} />}
        </View>
    );
}

const styles = StyleSheet.create({
    main: {
        paddingHorizontal: 10,
        marginVertical: 0,
        width: '100%'
    },
    mainPadding: {
        width: '95%',
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
