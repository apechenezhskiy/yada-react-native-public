import { Amplitude } from '@amplitude/react-native';
import Utils from '../services/utils';

export default class AmplitudeUtils {
    private static AMPLITUDE_API_KEY_DEV = `e1dc09ff2fa586eb1c5ba0e9886bda61`;
    private static AMPLITUDE_API_KEY_PROD = `29a8148d77025655d44b7dfcbbd2d742`;

    static initAmplitude = async () => {
        const ampInstance = Amplitude.getInstance();
        ampInstance.init((await Utils.isDevEnv()) ? this.AMPLITUDE_API_KEY_DEV : this.AMPLITUDE_API_KEY_PROD).then((result) => {
            if (result) {
                AmplitudeUtils.logEventAmpl('openApp');
            }
        });
    };

    static setUserID = (userID) => {
        Amplitude.getInstance().setUserId(userID);
    };

    static setUserProperties = (userProperties) => {
        Amplitude.getInstance().setUserProperties(userProperties);
    };

    static logEventAmpl = (eventName, eventProps?) => {
        Amplitude.getInstance().logEvent(eventName, eventProps);
    };
}
