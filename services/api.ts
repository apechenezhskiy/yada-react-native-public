import { Auth } from 'aws-amplify';
import 'react-native-url-polyfill/auto';
import { DefaultApi, Configuration, UserAnswer } from '../generated/';
import AmplitudeUtils from '../services/amplitudeUtils';
import { showToaster } from '../helpers/showToaster';
import { AxiosError } from 'axios';
import Utils from '../services/utils';

export default class Api {
    private static BASE_PATH_DEV = 'https://7pals7jst9.execute-api.eu-central-1.amazonaws.com/dev';
    private static BASE_PATH_PROD = 'https://api.yada.team';
    private static BASE_IMAGE_STOCK_URL = `https://pixabay.com/api`;
    private static BASE_IMAGE_STOCK_KEY = '8319320-a61e024d72c121ff197725de7';

    private static client: DefaultApi;

    private static isUserAuthenticated = false;

    static getAuthToken = async (): Promise<string> => {
        try {
            return (await Auth.currentSession()).getIdToken().getJwtToken();
        } catch (e) {
            return null;
        }
    };

    static buildClient = async () => {
        const token = await this.getAuthToken();
        const config = {
            basePath: (await Utils.isDevEnv()) ? this.BASE_PATH_DEV : this.BASE_PATH_PROD
        };
        this.isUserAuthenticated = token !== null;
        if (token) {
            config.apiKey = token;
        }

        return new DefaultApi(new Configuration(config));
    };

    static getApiClient = () => {
        if (!this.client) {
            this.client = this.buildClient();
        }

        return this.client;
    };

    static clearApiClient = () => {
        this.client = null;
    };

    static fetchImage = async (query: string) => {
        const url = `${this.BASE_IMAGE_STOCK_URL}?key=${this.BASE_IMAGE_STOCK_KEY}&q=${encodeURIComponent(
            query
        )}&per_page=3&orientation=horizontal&safesearch=true`;
        const response = await fetch(url);
        return await response.json();
    };

    private static BASE_PRONOUN_URL = `https://api.dictionaryapi.dev/api/v2/entries/en/`;

    static defaultCatchHandler = (err: any) => {
        const axiosError = err as AxiosError;

        if (axiosError.response) {
            const errorMessage = `API error\nstatus:${axiosError.response.status}\nbody:` + JSON.stringify(axiosError.response.data);
            showToaster(errorMessage);
        } else {
            showToaster(`Unexpected API error ${axiosError.code}`);
        }

        throw new Error(axiosError.message);
    };

    static fetchPronoun = async (word: string) => {
        const url = `${this.BASE_PRONOUN_URL}${word}`;
        const response = await fetch(url);
        return await response.json();
    };

    static lookupWord = async (prefix: string) => {
        const defaultApi = await this.getApiClient();

        AmplitudeUtils.logEventAmpl('lookupWord', {prefix});
        if (!this.isUserAuthenticated) {
            return defaultApi.publicLookupWord(prefix).catch(this.defaultCatchHandler);
        }

        return defaultApi.lookupWord(prefix).catch(this.defaultCatchHandler);
    };

    static getWord = async (id: number) => {
        const defaultApi = await this.getApiClient();
        if (!this.isUserAuthenticated) {
            return defaultApi.publicGetWord(id).catch(this.defaultCatchHandler);
        }

        return defaultApi.getWord(id).catch(this.defaultCatchHandler);
    };

    static getUserWords = async () => {
        const defaultApi = await this.getApiClient();
        return defaultApi.getDefaultWordList().catch(this.defaultCatchHandler);
    };

    static addUserWord = async (wordId: number, notes: string) => {
        const defaultApi = await this.getApiClient();
        AmplitudeUtils.logEventAmpl('addWordToMyWords', wordId);
        return defaultApi.addUserWord({ word_id: wordId, notes: notes }).catch(this.defaultCatchHandler);
    };

    static deleteUserWord = async (userwordId: number) => {
        const defaultApi = await this.getApiClient();
        AmplitudeUtils.logEventAmpl('removeWordFromMyWord', userwordId);
        return defaultApi.deleteUserWord(userwordId).catch(this.defaultCatchHandler);
    };

    static modifyUserWord = async (userWordId: number, wordId: number, notes: string) => {
        const defaultApi = await this.getApiClient();
        return defaultApi.modifyUserWord(userWordId, { word_id: wordId, notes: notes }).catch(this.defaultCatchHandler);
    };

    static getUserHistorySearches = async () => {
        const defaultApi = await this.getApiClient();
        return defaultApi.getUserSearchHistory().catch(this.defaultCatchHandler).catch(this.defaultCatchHandler);
    };

    static addUserHistorySearches = async (wordId: number) => {
        const defaultApi = await this.getApiClient();
        return defaultApi.addSearchHistoryRecord({ word_id: wordId }).catch(this.defaultCatchHandler);
    };

    static createWonderSession = async () => {
        const defaultApi = await this.getApiClient();
        AmplitudeUtils.logEventAmpl('startWonderSession');
        return defaultApi.createWonderSession().catch(this.defaultCatchHandler);
    };

    static saveUserAnswer = async (userAnswer: UserAnswer) => {
        const defaultApi = await this.getApiClient();
        AmplitudeUtils.logEventAmpl('saveUserAnswer', userAnswer);
        return defaultApi.saveUserAnswer(userAnswer).catch(this.defaultCatchHandler);
    };
}
