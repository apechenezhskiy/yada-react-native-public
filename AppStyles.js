import { DefaultTheme } from '@react-navigation/native';

export const AppStyles = {
    color: {
        primary: '#03a9f4',
        lightPrimary: '#63B9F4',
        lighterPrimary: '#33B9F4',
        lightAccent: '#78B7C9',
        background: '#fbf8f5',
        darkAccent: '#91555C',
        text: '#1b315d',
        secondaryText: '#757575',
        success: '#3DB07F',
        danger: '#F44336',
        placeholder: '#BDBDBD',
        main: '#5ea23a',
        title: '#464646',
        subtitle: '#545454',
        categoryTitle: '#161616',
        tint: '#ff5a66',
        description: '#bbbbbb',
        filterTitle: '#8a8a8a',
        starRating: '#2bdf85',
        location: '#a9a9a9',
        white: 'white',
        facebook: '#4267b2',
        grey: 'grey',
        greenBlue: '#00aea8',
        blue: '#3293fe'
    },
    fontSize: {
        title: 30,
        content: 20,
        normal: 16
    },
    buttonWidth: {
        main: '70%'
    },
    textInputWidth: {
        main: '80%'
    },
    borderRadius: {
        main: 10,
        small: 5
    }
};

export const CustomTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: AppStyles.color.primary,
        background: AppStyles.color.background,
        text: AppStyles.color.text,
        card: AppStyles.color.background
    }
};
