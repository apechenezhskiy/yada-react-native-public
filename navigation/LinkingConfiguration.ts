/**
 * Learn more about deep linking with React Navigation
 * https://reactnavigation.org/docs/deep-linking
 * https://reactnavigation.org/docs/configuring-links
 */

import * as Linking from 'expo-linking';

export default {
    prefixes: [Linking.makeUrl('/')],
    config: {
        screens: {
            Root: {
                screens: {
                    Root: {
                        screens: {
                            DictionaryTab: {
                                initialRouteName: 'Dictionary',
                                screens: {
                                    Dictionary: 'Dictionary',
                                    Word: 'Dictionary/Word/:id'
                                }
                            },
                            MyWordsTab: {
                                initialRouteName: 'MyWords',
                                screens: {
                                    MyWords: 'MyWords',
                                    Word: 'MyWords/Word/:id',
                                    WordsTrainer: 'MyWords/Trainer'
                                }
                            }
                        }
                    }
                }
            },
            SignUp: 'signup',
            SignOut: 'signout',
            ForgetPassword: 'forgetpassword',
            NotFound: '*'
        }
    }
};
