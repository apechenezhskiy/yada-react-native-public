/**
 * If you are not familiar with React Navigation, check out the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { NavigationContainer, DarkTheme, useNavigationContainerRef } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import React, { useState, useEffect } from 'react';
import { ColorSchemeName, StyleSheet } from 'react-native';
import { Auth, Hub } from 'aws-amplify';
import { CognitoUser } from '@aws-amplify/auth';

import BottomTabNavigator from './BottomTabNavigator';
import AuthContext from './AuthContext';

import NotFoundScreen from '../screens/NotFoundScreen';
import WelcomeScreen from '../screens/Welcome';
import SignInScreen from '../screens/SignIn';
import SignUpScreen from '../screens/SignUp';
import ForgetPassword from '../screens/ForgetPassword';
import SignOutScreen from '../screens/SignOut';
import SplashScreen from '../screens/Splash';
import { CustomTheme } from '../AppStyles';
import CustomSidebarMenu from '../components/CustomSidebarMenu';
import { RootStackParamList, AuthStackParamList, IdleStackParamList } from '../types';
import AmplitudeUtils from '../services/amplitudeUtils';
import Api from '../services/api';
import { AppStyles } from '../AppStyles';

export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
    const [userToken, setUserToken] = useState<string>(null);
    const [isUserTokenLoaded, setIsUserTokenLoaded] = useState<boolean>(false);

    const navigationRef = useNavigationContainerRef();
    const routeNameRef: React.MutableRefObject<string> = React.useRef();

    useEffect(() => {
        async function loadApp() {
            Hub.listen('auth', ({ payload: { event, data } }) => {
                switch (event) {
                    case 'signIn':
                    case 'cognitoHostedUI':
                        console.log('Hub signIn: ');
                        setUserToken(getTokenFromResponse(data));
                        Api.clearApiClient();
                        navigationRef.navigate('Dictionary');
                        break;
                    case 'signOut':
                        setUserToken(null);
                        Api.clearApiClient();
                        navigationRef.navigate('Dictionary', { isSignOut: true });
                        console.log('Hub signOut: ' + data);
                        break;
                }
            });

            // Add local store.
            getUser();
        }
        loadApp();
    }, []);

    const getTokenFromResponse = (response) => {
        AmplitudeUtils.setUserID(response?.username);
        return response?.signInUserSession?.accessToken?.jwtToken;
    };

    const getUser = async () => {
        Auth.currentAuthenticatedUser()
            .then((user: CognitoUser) => {
                const token = getTokenFromResponse(user) || user?.token;
                console.log('Success: ' + token);
                setUserToken(token);
            })
            .catch((e) => {
                console.log('err signing in ' + e);
            })
            .finally(() => {
                setIsUserTokenLoaded(true);
            });
    };

    const signOut = async () => {
        await Auth.signOut().catch((err) => {
            console.log('ERROR: ', err);
        });
    };

    const getViewEventString = (current) => {
        return `view${current}`;
    };

    const getViewEventParamsObject = (previousRouteName) => {
        const result = { prevScreen: previousRouteName };
        const currentRouteInfo = navigationRef.getCurrentRoute();
        if (currentRouteInfo?.name === 'Word' && currentRouteInfo.params.id) {
            result.wordId = currentRouteInfo.params.id;
        }
        return result;
    };

    const onNavigationContainerReady = React.useCallback(() => {
        routeNameRef.current = navigationRef.getCurrentRoute().name;
    }, [navigationRef]);

    const onNavigationContainerStateChange = React.useCallback(async () => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = navigationRef.getCurrentRoute().name;

        if (previousRouteName !== currentRouteName) {
            AmplitudeUtils.logEventAmpl(getViewEventString(currentRouteName), getViewEventParamsObject(previousRouteName));
        }

        routeNameRef.current = currentRouteName;
    }, [routeNameRef, routeNameRef.current]);

    return (
        <AuthContext.Provider value={{ signOut, userToken }}>
            <NavigationContainer
                ref={navigationRef}
                onReady={onNavigationContainerReady}
                onStateChange={onNavigationContainerStateChange}
                theme={colorScheme === 'dark' ? DarkTheme : CustomTheme}
            >
                {isUserTokenLoaded ? <DrawerNavigator /> : <RootNavigatorIdle />}
            </NavigationContainer>
        </AuthContext.Provider>
    );
}

function DrawerNavigator() {
    const Drawer = createDrawerNavigator();
    return (
        <Drawer.Navigator drawerContent={(props) => <CustomSidebarMenu {...props} />} screenOptions={{ drawerType: 'slide', headerShown: false }}>
            <Drawer.Screen name={'DrawerRoot'} component={RootNavigator} options={{ drawerLabel: 'Dictionary', drawerLabelStyle: styles.menuItem }} />
        </Drawer.Navigator>
    );
}

function RootNavigator() {
    // A root stack navigator is often used for displaying modals on top of all other content
    // Read more here: https://reactnavigation.org/docs/modal
    const Stack = createStackNavigator<RootStackParamList>();

    return (
        <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName={'Root'}>
            <Stack.Screen name={'Root'} component={BottomTabNavigator} />
            <Stack.Screen name={'NotFound'} component={NotFoundScreen} options={{ title: 'Oops!' }} />
            <Stack.Screen name={'SignOut'} component={SignOutScreen} />
            <Stack.Screen name={'Welcome'} component={WelcomeScreen} options={{ headerShown: true, headerBackTitle: '', title: '' }} />
            <Stack.Screen name={'SignIn'} component={SignInScreen} options={{ ...authHeaderStyle, title: 'Sign In', headerBackTitleVisible: true }} />
            <Stack.Screen
                name={'SignUp'}
                component={SignUpScreen}
                options={{ ...authHeaderStyle, title: 'Create new account', headerBackTitleVisible: false }}
            />
            <Stack.Screen
                name={'ForgetPassword'}
                component={ForgetPassword}
                options={{ ...authHeaderStyle, title: 'Forget password', headerBackTitleVisible: false }}
            />
        </Stack.Navigator>
    );
}

const authHeaderStyle = {
    headerTitle: '',
    headerStyle: {
        backgroundColor: AppStyles.color.primary,
        shadowColor: 'transparent'
    },
    headerTitleStyle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18
    },
    headerTintColor: AppStyles.color.white
};

function RootNavigatorAuth() {
    const AuthStack = createStackNavigator<AuthStackParamList>();

    return (
        <AuthStack.Navigator screenOptions={{ cardStyle: { backgroundColor: '#ff3333' } }}>
            <AuthStack.Screen name={'Welcome'} component={WelcomeScreen} options={{ headerShown: false }} />
            <AuthStack.Screen
                name={'SignIn'}
                component={SignInScreen}
                options={{ ...authHeaderStyle, title: 'Sign In', headerBackTitleVisible: false }}
            />
            <AuthStack.Screen
                name={'SignUp'}
                component={SignUpScreen}
                options={{ ...authHeaderStyle, title: 'Create new account', headerBackTitleVisible: false }}
            />
            <AuthStack.Screen
                name={'ForgetPassword'}
                component={ForgetPassword}
                options={{ ...authHeaderStyle, title: 'Forget password', headerBackTitleVisible: false }}
            />
        </AuthStack.Navigator>
    );
}

function RootNavigatorIdle() {
    const StackIdle = createStackNavigator<IdleStackParamList>();

    return (
        <StackIdle.Navigator screenOptions={{ headerShown: false }} initialRouteName={'Root'}>
            <StackIdle.Screen name={'Root'} component={SplashScreen} />
        </StackIdle.Navigator>
    );
}

const styles = StyleSheet.create({
    menuItem: {
        fontSize: 22,
        lineHeight: 70,
        color: AppStyles.color.white,
        fontWeight: '400'
    }
});
