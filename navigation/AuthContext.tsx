import React from 'react';

export interface AppAuthContextInterface {
    userToken: string;
    signOut: () => void;
}

export const AuthContext = React.createContext<AppAuthContextInterface>({
    userToken: null,
    signOut: () => {
        return;
    }
});

export default AuthContext;
