/**
 * Learn more about createBottomTabNavigator:
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import DictionaryScreen from '../screens/Dictionary';
import MyWordsScreen from '../screens/MyWords';
import WordScreen from '../screens/Word';
import WordsTrainerScreen from '../screens/WordsTrainer';
import WordsTrainerResult from '../screens/WordsTrainerResult';
import { VectorIcon } from '../components/VectorIcon';
import { BottomTabParamList, DictionaryTabParamList, MyWordsTabParamList } from '../types';
import { AppStyles } from '../AppStyles';
import { TouchableOpacity } from 'react-native-gesture-handler';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
    const colorScheme = useColorScheme();

    return (
        <BottomTab.Navigator
            initialRouteName={'DictionaryTab'}
            screenOptions={{
                tabBarActiveTintColor: Colors[colorScheme].tint,
                headerShown: false
            }}
        >
            <BottomTab.Screen
                name={'DictionaryTab'}
                component={DictionaryTabNavigator}
                options={{
                    tabBarLabel: 'Dictionary',
                    tabBarIcon: () => <VectorIcon size={30} name={'book-outline'} color={AppStyles.color.primary} />
                }}
            />
            <BottomTab.Screen
                name={'MyWordsTab'}
                component={MyWordsTabNavigator}
                options={{
                    tabBarLabel: 'My Words',
                    tabBarIcon: () => <VectorIcon size={30} name={'bookmarks-outline'} color={AppStyles.color.primary} />
                }}
            />
        </BottomTab.Navigator>
    );
}

const DictionaryTabStack = createStackNavigator<DictionaryTabParamList>();

const TabStackScreenOptions = (title, navigation, isBackButtonVisible) => ({
    headerTitle: title,
    headerStyle: {
        backgroundColor: AppStyles.color.primary,
        shadowColor: 'transparent'
    },
    headerTitleStyle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18
    },
    headerLeft: () => (isBackButtonVisible ? headerLeftMenuIconGoBack(navigation) : headerLeftMenuIconToggle(navigation))
});

const headerLeftMenuIconToggle = (navigation) => {
    const showMenu = React.useCallback(() => {
        navigation.toggleDrawer();
    }, []);

    return (
        <TouchableOpacity style={{ marginLeft: 24 }} onPress={showMenu}>
            <VectorIcon size={24} name={'menu'} color={AppStyles.color.white} />
        </TouchableOpacity>
    );
};

const headerLeftMenuIconGoBack = (navigation) => {
    const showMenu = React.useCallback(() => {
        navigation.goBack();
    }, []);

    return (
        <TouchableOpacity style={{ marginLeft: 24 }} onPress={showMenu}>
            <VectorIcon size={24} name={'chevron-back'} color={AppStyles.color.white} />
        </TouchableOpacity>
    );
};

function DictionaryTabNavigator({ navigation }) {
    return (
        <DictionaryTabStack.Navigator>
            <DictionaryTabStack.Screen
                name={'Dictionary'}
                component={DictionaryScreen}
                options={TabStackScreenOptions('Dictionary', navigation, false)}
            />
            <DictionaryTabStack.Screen name={'Word'} component={WordScreen} options={TabStackScreenOptions('', navigation, true)} />
        </DictionaryTabStack.Navigator>
    );
}

const MyWordsTabStack = createStackNavigator<MyWordsTabParamList>();

function MyWordsTabNavigator({ navigation }) {
    return (
        <MyWordsTabStack.Navigator>
            <MyWordsTabStack.Screen name={'MyWords'} component={MyWordsScreen} options={TabStackScreenOptions('My Words', navigation, false)} />
            <MyWordsTabStack.Screen
                name={'Word'}
                component={WordScreen}
                options={{
                    headerTitle: '',
                    headerStyle: {
                        backgroundColor: AppStyles.color.primary
                    },
                    headerTitleStyle: {
                        fontWeight: 'bold',
                        fontSize: 18,
                        color: 'white'
                    },
                    headerTintColor: AppStyles.color.white,
                    headerBackTitleVisible: false
                }}
            />
            <MyWordsTabStack.Screen
                name={'WordsTrainer'}
                component={WordsTrainerScreen}
                options={{
                    headerTitle: 'Word Trainer',
                    headerStyle: {
                        backgroundColor: AppStyles.color.primary
                    },
                    headerTitleStyle: {
                        fontWeight: 'bold',
                        fontSize: 18,
                        color: 'white'
                    },
                    headerTintColor: AppStyles.color.white,
                    headerBackTitleVisible: false
                }}
            />
            <MyWordsTabStack.Screen
                name={'WordsTrainerResult'}
                component={WordsTrainerResult}
                options={{
                    headerTitle: 'Word Trainer Result',
                    headerStyle: {
                        backgroundColor: AppStyles.color.primary
                    },
                    headerTitleStyle: {
                        fontWeight: 'bold',
                        fontSize: 18,
                        color: 'white'
                    },
                    headerTintColor: AppStyles.color.white,
                    headerBackTitleVisible: false
                }}
            />
        </MyWordsTabStack.Navigator>
    );
}
