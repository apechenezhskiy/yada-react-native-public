import * as React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';

import { Text } from './Themed';
import { ButtonProps } from '../types';

export function Button(props: ButtonProps) {
    const theme = useTheme();
    const styles = React.useMemo(() => createStyles(theme), [theme]);

    return (
        <TouchableOpacity {...props} onPress={props.onPress} style={[styles.buttonContainer]}>
            <Text style={styles.buttonText}>{props.text}</Text>
        </TouchableOpacity>
    );
}
const createStyles = (theme: Theme) =>
    StyleSheet.create({
        buttonContainer: {
            backgroundColor: theme.colors.primary,
            width: '85%',
            height: 60,
            marginTop: 10,
            marginBottom: 10,
            borderRadius: 8,
            alignItems: 'center',
            justifyContent: 'center'
        },
        buttonText: {
            fontSize: 18,
            lineHeight: 28,
            color: theme.colors.background
        }
    });
