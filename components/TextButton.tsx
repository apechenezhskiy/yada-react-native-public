import * as React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';

import { Text } from './Themed';
import { ButtonProps } from '../types';

export function TextButton(props: ButtonProps) {
    const theme = useTheme();
    const styles = React.useMemo(() => createStyles(theme), [theme]);

    return (
        <TouchableOpacity onPress={props.onPress}>
            <Text style={styles.buttonText}>{props.text}</Text>
        </TouchableOpacity>
    );
}
const createStyles = (theme: Theme) =>
    StyleSheet.create({
        buttonText: {
            fontSize: 14,
            color: theme.colors.primary,
            lineHeight: 21,
            fontWeight: '500',
            textAlign: 'center'
        }
    });
