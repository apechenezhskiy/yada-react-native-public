import * as React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';
import { Meaning } from '../generated/';
import { AppStyles } from '../AppStyles';

interface MeaningProps {
    meaning: Meaning;
}

export function MeaningWrapper({ meaning }: MeaningProps) {
    const [examplesNumberOfLines, setExamplesNumberOfLines] = React.useState(5);

    const theme = useTheme();
    const styles = React.useMemo(() => createStyles(theme), [theme]);

    const examplesOnPressHandler = React.useCallback(() => {
        setExamplesNumberOfLines(0);
    }, [meaning]);

    return (
        <View key={meaning.id} style={styles.wordDefMainCardView}>
            <View style={styles.wordDefInnerCardView}>
                <Text style={styles.row}>
                    <Text style={styles.partOfSpeech}>{'(' + meaning.part_of_speech + ') '} </Text>
                    {meaning.definition}
                </Text>
                {meaning.examples.length > 0 &&
                    (examplesNumberOfLines ? (
                        <TouchableOpacity onPress={examplesOnPressHandler}>
                            <Text numberOfLines={examplesNumberOfLines} style={styles.wordExamples}>
                                <Text style={styles.boldText}>Example:</Text> {meaning.examples.join(`;\n`)}
                            </Text>
                        </TouchableOpacity>
                    ) : (
                        <Text numberOfLines={examplesNumberOfLines} style={styles.wordExamples}>
                            <Text style={styles.boldText}>Example:</Text> {meaning.examples.join(`;\n`)}
                        </Text>
                    ))}
                {meaning.synonyms.length > 0 && (
                    <Text style={styles.wordExamples}>
                        <Text style={styles.boldText}>Synonyms:</Text> {meaning.synonyms.join(`; `)}
                    </Text>
                )}
                {meaning.antonyms.length > 0 && (
                    <Text style={styles.wordExamples}>
                        <Text style={styles.boldText}>Antonyms:</Text> {meaning.antonyms.join(`; `)}
                    </Text>
                )}
            </View>
        </View>
    );
}

const createStyles = (theme: Theme) =>
    StyleSheet.create({
        row: {
            flexDirection: 'row',
            fontSize: 16,
        },
        wordDefMainCardView: {
            alignItems: 'center',
            backgroundColor: AppStyles.color.white,
            borderRadius: 8,
            boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.04), 0px 0px 2px rgba(0, 0, 0, 0.06), 0px 0px 1px rgba(0, 0, 0, 0.04)',
            elevation: 8,
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingLeft: 16,
            paddingRight: 14,
            marginTop: 6,
            marginBottom: 6
        },
        wordDefInnerCardView: {
            paddingTop: 10,
            paddingBottom: 10,
            alignSelf: 'center',
            alignItems: 'flex-start',
            width: '100%',
        },
        wordExamples: {
            paddingTop: 5,
        },
        boldText: {
            fontWeight: 'bold'
        },
        partOfSpeech: {
            fontWeight: 'bold',
            fontSize: 13
        }
    });
