import * as React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';

import { VectorIcon } from './VectorIcon';
import { AppStyles } from '../AppStyles';
import { IsMyWordIconProps } from '../types';

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
export function IsMyWordIcon({ hasUserWord, onPressCallback, size = 20, color = AppStyles.color.primary }: IsMyWordIconProps) {
    const theme = useTheme();
    const styles = React.useMemo(() => createStyles(theme), [theme]);

    return (
        <TouchableOpacity style={styles.addWordIcon} onPress={onPressCallback}>
            <VectorIcon size={size} name={hasUserWord ? 'bookmark' : 'bookmark-outline'} color={color} />
        </TouchableOpacity>
    );
}

const createStyles = (theme: Theme) =>
    StyleSheet.create({
        addWordIcon: {
            alignItems: 'center',
            justifyContent: 'center',
            marginRight: '1%'
        }
    });
