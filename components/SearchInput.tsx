import * as React from 'react';
import { StyleSheet } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';

import { Input } from '../components/Input';
import { View } from '../components/Themed';
import { AppStyles } from '../AppStyles';

export function SearchInput({ inputWord, setLookupInputWord, clearInput }) {
    const theme = useTheme();
    const styles = React.useMemo(() => createStyles(theme), [theme]);

    return (
        <View style={[styles.searchInputWrapper]}>
            <Input
                style={styles.searchInput}
                wrapperStyle={styles.searchInputContainer}
                isLeftIcon={true}
                placeholderTextColor={'white'}
                maxLength={64}
                placeholder={'Search for Word'}
                value={inputWord}
                showClearButton={true}
                clearButtonPressed={clearInput}
                onChangeText={setLookupInputWord}
            />
        </View>
    );
}
const createStyles = (theme: Theme) =>
    StyleSheet.create({
        searchInputWrapper: {
            height: 126,
            backgroundColor: AppStyles.color.primary,
            width: '100%',
            paddingBottom: 24,
            paddingTop: 24
        },
        searchInput: {
            fontSize: 18,
            width: '87%',
            height: 68,
            paddingLeft: 28,
            color: AppStyles.color.white
        },
        searchInputContainer: {
            backgroundColor: AppStyles.color.lightPrimary,
            borderWidth: 0,
            borderRadius: 8,
            width: '87%'
        }
    });
