import React from 'react';
import { SafeAreaView, View, StyleSheet, Text } from 'react-native';
import { version } from '../package.json';

import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import { useNavigation, CommonActions } from '@react-navigation/native';
import { Auth } from 'aws-amplify';
import { AppStyles } from '../AppStyles';
import { VectorIcon } from './VectorIcon';
import Utils from '../services/utils';
import { AuthContext, AppAuthContextInterface } from '../navigation/AuthContext';

const CustomSidebarMenu = (props) => {
    const [versionText, setVersionText] = React.useState('');
    const { userToken } = React.useContext<AppAuthContextInterface>(AuthContext) as AppAuthContextInterface;

    React.useEffect(() => {
        async function getVersion() {
            const envName = await Utils.getEnvName();
            if (envName !== 'PRODUCTION') {
                setVersionText('Version:' + version + ' ' + envName);
            }
        }

        getVersion();
    }, []);

    const navigation = useNavigation();
    const signOut = React.useCallback(async () => {
        await Auth.signOut().catch((err) => {
            console.log('ERROR: ', err);
        });
    }, []);

    const signIn = React.useCallback(() => {
        navigation.dispatch(
            CommonActions.navigate({
                name: 'Welcome'
            })
        );
    }, []);

    const goToMyWords = React.useCallback(() => {
        navigation.dispatch(
            CommonActions.navigate({
                name: 'MyWordsTab'
            })
        );
    }, []);

    const goToDictionary = React.useCallback(() => {
        navigation.dispatch(
            CommonActions.navigate({
                name: 'DictionaryTab'
            })
        );
    }, []);

    const getIconJSX = (iconName) => {
        return <VectorIcon size={30} name={iconName} color={AppStyles.color.white} />;
    };

    const isAuthUser = () => {
        return userToken !== null;
    };

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: AppStyles.color.primary }}>
            <Text style={styles.yadaText}>Yada</Text>
            <DrawerContentScrollView {...props}>
                <DrawerItem icon={() => getIconJSX('book-outline')} labelStyle={styles.menuItem} label={'Dictionary'} onPress={goToDictionary} />
                <View style={styles.itemSeparator} />
                <DrawerItem icon={() => getIconJSX('bookmarks-outline')} labelStyle={styles.menuItem} label={'My Words'} onPress={goToMyWords} />
            </DrawerContentScrollView>

            {isAuthUser() ? (
                <DrawerItem icon={() => getIconJSX('log-out-outline')} labelStyle={styles.menuItem} label={'Sign Out'} onPress={signOut} />
            ) : (
                <DrawerItem icon={() => getIconJSX('log-out-outline')} labelStyle={styles.menuItem} label={'Sign In'} onPress={signIn} />
            )}
            <Text style={styles.versionText}>{versionText}</Text>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    sideMenuProfileIcon: {
        resizeMode: 'center',
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        alignSelf: 'center'
    },
    customItem: {
        padding: 16,
        flexDirection: 'row',
        alignItems: 'center'
    },
    menuItem: {
        fontSize: 22,
        lineHeight: 70,
        color: AppStyles.color.white,
        fontWeight: '400'
    },
    itemSeparator: {
        height: 1,
        width: '100%',
        backgroundColor: 'rgba(0,0,0,0.1)',
        alignSelf: 'center'
    },
    yadaText: {
        fontWeight: 'bold',
        fontSize: 38,
        color: AppStyles.color.white,
        textAlign: 'center',
        marginTop: 70
    },
    versionText: {
        fontSize: 12,
        alignSelf: 'center',
        color: AppStyles.color.white
    }
});

export default CustomSidebarMenu;
