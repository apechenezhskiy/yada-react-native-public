/**
 * Learn more about Light and Dark modes:
 * https://docs.expo.io/guides/color-schemes/
 */

import React from 'react';
import { Text as DefaultText, View as DefaultView, StyleSheet } from 'react-native';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import { AppStyles } from '../AppStyles';
import { ThemeProps } from '../types';

export function useThemeColor(props: { light?: string; dark?: string }, colorName: keyof typeof Colors.light & keyof typeof Colors.dark) {
    const theme = useColorScheme();
    const colorFromProps = props[theme];

    if (colorFromProps) {
        return colorFromProps;
    } else {
        return Colors[theme][colorName];
    }
}

export type TextProps = ThemeProps & DefaultText['props'];
export type ViewProps = ThemeProps & DefaultView['props'];

export function Text(props: TextProps) {
    const { style, lightColor, darkColor, ...otherProps } = props;
    const color = useThemeColor({ light: lightColor, dark: darkColor }, 'text');

    return <DefaultText style={[{ color }, style]} {...otherProps} />;
}

export function ErrorText({ style, text, ...otherProps }: TextProps) {
    const color = AppStyles.color.danger;

    if (text === '') {
        return null;
    }

    return (
        <DefaultText style={[{ margin: 0, color, alignSelf: 'flex-start', marginHorizontal: '10%', fontSize: 12 }, style]} {...otherProps}>
            {text}
        </DefaultText>
    );
}

export function View(props: ViewProps) {
    const { style, lightColor, darkColor, ...otherProps } = props;
    const backgroundColor = useThemeColor({ light: AppStyles.color.background, dark: darkColor }, 'background');

    return <DefaultView style={[{ backgroundColor }, style]} {...otherProps} />;
}

export function BaseView(props: ViewProps) {
    return <View style={styles.container} {...props} />;
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20
    },
    flashMessage: {
        position: 'absolute',
        backgroundColor: AppStyles.color.danger,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        top: 0
    },
    flashMessageText: {
        color: AppStyles.color.background
    }
});
