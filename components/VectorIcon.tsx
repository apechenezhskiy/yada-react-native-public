import * as React from 'react';
import { StyleSheet } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';

import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

export function VectorIcon(props: {
    name: React.ComponentProps<typeof IconIonicons>['name'] & React.ComponentProps<typeof IconFontAwesome>['name'];
    color: string;
    size: number;
    iconSet?: string;
}) {
    const theme = useTheme();
    const styles = React.useMemo(() => createStyles(theme), [theme]);

    return props.iconSet === 'FontAwesome' ? (
        <IconFontAwesome style={styles.iconsStyle} {...props} />
    ) : (
        <IconIonicons style={styles.iconsStyle} {...props} />
    );
}

const createStyles = (theme: Theme) =>
    StyleSheet.create({
        iconsStyle: {}
    });
