import * as React from 'react';
import { StyleSheet, ActivityIndicator } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';

import { View } from './Themed';
import { LoadingProps } from '../types';

export function Loading(props: LoadingProps) {
    const theme = useTheme();
    const styles = React.useMemo(() => createStyles(theme), [theme]);
    const { isVisible } = props;

    return isVisible ? (
        <View style={styles.loading}>
            <ActivityIndicator size={'large'} />
        </View>
    ) : null;
}
const createStyles = (theme: Theme) =>
    StyleSheet.create({
        loading: {
            position: 'absolute',
            opacity: 0.5,
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center'
        }
    });
