import React from 'react';
import { Text, TouchableOpacity, StyleSheet, Image, Dimensions } from 'react-native';
import { AppStyles } from '../../AppStyles';

const screen = Dimensions.get('screen');

const styles = StyleSheet.create({
    googleStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#ffffff',
        borderWidth: 0.5,
        borderColor: '#fff',
        height: 60,
        borderRadius: 5,
        margin: 5,
        width: screen.width * 0.8
    },
    imageIconStyle: {
        padding: 10,
        marginLeft: 20,
        height: 24,
        width: 24,
        resizeMode: 'stretch'
    },
    mailIconStyle: {
        height: 18,
        width: 22
    },
    textStyle: {
        color: AppStyles.color.text,
        fontWeight: '500',
        fontSize: 14,
        marginLeft: 25,
        marginRight: 20
    }
});

export function SocialButton({ onPress, source, buttonText }) {
    return (
        <TouchableOpacity style={{ ...styles.googleStyle }} onPress={onPress}>
            <Image source={source} style={[{ ...styles.imageIconStyle }, buttonText === 'Sign in with Email' ? styles.mailIconStyle : null]} />
            <Text style={{ ...styles.textStyle }}>{buttonText || 'Sign in with Google'}</Text>
        </TouchableOpacity>
    );
}
