import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';
import { MeaningWrapper } from '../components/MeaningWrapper';
import { Etymology, Meaning } from '../generated/';

interface EtymologyProps {
    etymology: Etymology;
}

export function EtymologyWrapper({ etymology }: EtymologyProps) {
    const theme = useTheme();
    const styles = React.useMemo(() => createStyles(theme), [theme]);

    return (
        <View>
            <View style={styles.row}>
                <Text style={[styles.title]}></Text>
            </View>
            {etymology.meanings &&
                etymology.meanings.length > 0 &&
                etymology.meanings.map((meaning: Meaning) => {
                    return <MeaningWrapper meaning={meaning} key={meaning.id} />;
                })}
        </View>
    );
}

const createStyles = (theme: Theme) =>
    StyleSheet.create({
        title: {
            fontSize: 14,
            fontWeight: 'bold',
            color: theme.colors.text
        },
        row: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 0
        }
    });
