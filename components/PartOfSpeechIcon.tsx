import * as React from 'react';

import { VectorIcon } from './VectorIcon';

const partOfSpeechIconsDescription = {
    noun: {
        size: 15,
        name: 'triangle',
        color: 'black'
    },
    verb: {
        size: 15,
        name: 'circle',
        color: 'red',
        iconSet: 'FontAwesome'
    },
    adj: {
        size: 12,
        name: 'triangle',
        color: 'blue'
    },
    det: {
        size: 12,
        name: 'triangle',
        color: 'lightblue'
    },
    adverb: {
        size: 12,
        name: 'circle',
        color: 'orange',
        iconSet: 'FontAwesome'
    },
    pron: {
        size: 12,
        name: 'triangle',
        color: 'purple'
    },
    prep: {
        size: 12,
        name: 'circle',
        color: 'green',
        iconSet: 'FontAwesome'
    },
    conj: {
        size: 12,
        name: 'square',
        color: 'pink'
    },
    intj: {
        size: 12,
        name: 'square',
        color: 'yellow'
    }
};

export function PartOfSpeechIcon({ type }: any) {
    if (!partOfSpeechIconsDescription.hasOwnProperty(type)) {
        return null;
    }

    const descrition = partOfSpeechIconsDescription[type];

    return <VectorIcon size={descrition.size} name={descrition.name} color={descrition.color} iconSet={descrition.iconSet} />;
}
