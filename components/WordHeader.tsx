import * as React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, ScaledSize } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';
import Sound from 'react-native-sound';
import { IsMyWordIcon } from '../components/IsMyWordIcon';
import { VectorIcon } from '../components/VectorIcon';
import { CustomImage } from '../components/CustomImage';
import { Word, WordListEntry } from '../generated/';
import Api from '../services/api';
import { AppStyles } from '../AppStyles';
import AmplitudeUtils from '../services/amplitudeUtils';

interface WordHeaderProps {
    word: Word | WordListEntry;
    showAddToFavIcon?: boolean;
    setIsLoading: (isLoading: boolean) => void;
    showWordCard?: (card: number) => void;
    headerColor?: string;
    onWordChanged?: (isAdded: boolean, userwordId?: number) => void;
    showRightIcon?: boolean;
}

const WIDTH_RATIO = 0.9;

export function WordHeader({ word, showAddToFavIcon, setIsLoading, showWordCard, headerColor, onWordChanged, showRightIcon }: WordHeaderProps) {
    const [wordImageResponse, setWordImageResponse] = React.useState(null);

    const theme = useTheme();
    const win = Dimensions.get('window');
    const styles = React.useMemo(() => createStyles(theme, win), [theme, win]);

    const playAudio = React.useCallback(() => {
        const track = new Sound(word.audio_url, null, (e) => {
            if (e) {
                console.log('error loading track:', e);
            } else {
                AmplitudeUtils.logEventAmpl('listenAudio', word.id);
                track.play((success) => console.log('track playing success'));
            }
        });
    }, [word]);

    const setImages = (images: any) => {
        setWordImageResponse(images?.hits?.length ? images.hits[0] : null);
    };

    React.useEffect(() => {
        async function loadImage() {
            Api.fetchImage(word.word)
                .then(setImages)
                .finally(() => setIsLoading(false));
        }
        loadImage();
    }, [word]);

    const addUserWord = () => {
        setIsLoading(true);
        Api.addUserWord(word.id, '')
            .then(({ data }) => {
                if (onWordChanged) {
                    onWordChanged(true, data.id);
                }
            })
            .finally(() => setIsLoading(false));
    };

    const removeUserWord = () => {
        setIsLoading(true);
        Api.deleteUserWord(word.userword_id)
            .then(({ data }) => {
                if (onWordChanged) {
                    onWordChanged(false);
                }
            })
            .finally(() => setIsLoading(false));
    };

    const onShowWord = React.useCallback(() => {
        showWordCard(word.id);
    }, [word]);

    const imageHeight = wordImageResponse ? (win.width * WIDTH_RATIO * wordImageResponse.webformatHeight) / wordImageResponse.webformatWidth : 50;

    return (
        <View
            style={[
                { ...styles.wordHeader, backgroundColor: 'transparent', height: imageHeight },
                !wordImageResponse && styles.wordHeaderWithoutImage
            ]}
        >
            {wordImageResponse && (
                <View style={{ ...styles.container, position: 'relative' }}>
                    <CustomImage source={wordImageResponse?.webformatURL} style={{ ...styles.wordImageResponseImage, height: imageHeight }} />
                </View>
            )}
            <View style={[styles.relativeBoxText, wordImageResponse && styles.relativeBoxTextWithImage]}>
                <View style={[styles.wordHeaderCellTitle, wordImageResponse && styles.lightAccentBackground]}>
                    <Text style={[styles.wordTitle]}>{word.word}</Text>
                    {word.phonetic !== '' && typeof word.phonetic === 'string' && (
                        <View style={[styles.wordPronunciation, wordImageResponse && styles.lightAccentBackground]}>
                            <Text style={styles.wordTranscription}>{word.phonetic}</Text>
                            {word.audio_url !== '' && (
                                <TouchableOpacity onPress={playAudio}>
                                    <VectorIcon size={20} name={'play-circle-outline'} color={AppStyles.color.text} />
                                </TouchableOpacity>
                            )}
                        </View>
                    )}
                </View>
                {showRightIcon && (
                    <View style={[styles.wordHeaderCellButton, wordImageResponse && styles.lightAccentBackground]}>
                        {showAddToFavIcon && (
                            <IsMyWordIcon
                                size={30}
                                hasUserWord={word.is_added}
                                onPressCallback={word.is_added ? removeUserWord : addUserWord}
                                color={wordImageResponse ? headerColor : AppStyles.color.primary}
                            />
                        )}
                        {!showAddToFavIcon && (
                            <TouchableOpacity onPress={onShowWord}>
                                <VectorIcon size={30} name={'chevron-down'} color={AppStyles.color.text} />
                            </TouchableOpacity>
                        )}
                    </View>
                )}
            </View>
        </View>
    );
}

const createStyles = (theme: Theme, win: ScaledSize) =>
    StyleSheet.create({
        container: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center'
        },
        wordHeader: {
            marginBottom: 5,
            alignSelf: 'center'
        },
        wordHeaderWithoutImage: {
            marginTop: 15
        },
        relativeBoxTextWithImage: {
            borderTopLeftRadius: 0,
            borderTopRightRadius: 0,
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0
        },
        relativeBoxText: {
            flexDirection: 'row',
            borderRadius: 20,
            overflow: 'hidden',
            color: AppStyles.color.text,
            width: win.width * WIDTH_RATIO
        },
        wordHeaderCellTitle: {
            alignItems: 'center',
            textAlign: 'center',
            width: '100%',
            paddingBottom: 5
        },
        wordHeaderCellButton: {
            left: -40,
            alignSelf: 'center',
            width: 30
        },
        lightAccentBackground: {
            backgroundColor: AppStyles.color.primary
        },
        wordTitle: {
            fontSize: 30,
            fontWeight: 'bold',
            borderRadius: 200
        },
        wordTranscription: {
            fontSize: 16,
            marginRight: 2
        },
        wordPronunciation: {
            flexDirection: 'row',
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%'
        },
        wordImageResponseImage: {
            borderRadius: 20,
            borderWidth: 2,
            borderColor: AppStyles.color.lightAccent,
            width: win.width * WIDTH_RATIO
        }
    });
