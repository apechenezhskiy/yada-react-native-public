import React, { useState, useCallback } from 'react';
import { ActivityIndicator, View, StyleSheet } from 'react-native';
import { isEmpty } from 'lodash';
import FastImage from 'react-native-fast-image';
import { AppStyles } from '../AppStyles';

export const CustomImage = (props) => {
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);
    const { source = '', style, defaultImage, size = 'large', resizeMode = 'cover', ...restProps } = props;

    const loadingTurnOn = useCallback(() => {
        setLoading(true);
    }, []);

    const loadingTurnOff = useCallback(() => {
        setLoading(false);
    }, []);

    const setErrorTrue = useCallback(() => {
        setError(true);
    }, []);

    return (
        <FastImage
            source={isEmpty(source) || error ? defaultImage : { uri: source }}
            {...restProps}
            style={[styles.image, style]}
            onLoadStart={loadingTurnOn}
            onLoadEnd={loadingTurnOff}
            {...restProps}
            onError={setErrorTrue}
        >
            {loading && (
                <View style={[styles.loader, style]}>
                    <ActivityIndicator size={size} color={AppStyles.color.primary} />
                </View>
            )}
        </FastImage>
    );
};

const styles = StyleSheet.create({
    image: {
        width: 100,
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: AppStyles.color.white
    },
    loader: {
        alignItems: 'center',
        justifyContent: 'center'
    }
});
