import * as React from 'react';
import { StyleSheet } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';

import { Text, View } from '../components/Themed';
import { AppStyles } from '../AppStyles';

export function FlatListItem({ children, isLastItem }: { key: number; isLastItem: boolean; children: React.ReactNode }) {
    const theme = useTheme();
    const styles = React.useMemo(() => createStyles(theme), [theme]);

    return (
        <View style={styles.wordListRow}>
            {children}
            {!isLastItem && <View style={styles.itemSeparator} />}
        </View>
    );
}
const createStyles = (theme: Theme) =>
    StyleSheet.create({
        wordListRow: {
            width: '100%',
            height: 64,
            backgroundColor: AppStyles.color.white
        },
        itemSeparator: {
            height: 1,
            width: '100%',
            backgroundColor: 'rgba(0,0,0,0.1)',
            alignSelf: 'center'
        }
    });
