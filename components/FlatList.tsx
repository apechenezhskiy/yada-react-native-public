import * as React from 'react';
import { StyleSheet } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';

import { Text, View } from '../components/Themed';
import { AppStyles } from '../AppStyles';

export function FlatList({ headerText, children }: { headerText?: string; children: React.ReactNode }) {
    const theme = useTheme();
    const styles = React.useMemo(() => createStyles(theme), [theme]);

    return (
        <View style={styles.flatListWrapper}>
            {headerText && <Text style={styles.labelText}>{headerText}</Text>}
            <View style={styles.wordsListWrapper}>{children}</View>
        </View>
    );
}
const createStyles = (theme: Theme) =>
    StyleSheet.create({
        flatListWrapper: {
            width: '90%'
        },
        wordsListWrapper: {
            alignItems: 'center',
            backgroundColor: AppStyles.color.white,
            borderRadius: 8,
            boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.04), 0px 0px 2px rgba(0, 0, 0, 0.06), 0px 0px 1px rgba(0, 0, 0, 0.04)',
            elevation: 8,
            flexDirection: 'column',
            justifyContent: 'space-between',
            paddingLeft: 16,
            paddingRight: 14,
            marginTop: 15,
            marginBottom: 6
        },
        labelText: {
            fontSize: 16,
            color: AppStyles.color.primary,
            fontWeight: 'bold',
            marginLeft: 0,
            marginRight: 'auto',
            marginTop: 10
        }
    });
