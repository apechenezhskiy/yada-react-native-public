import * as React from 'react';
import { StyleSheet } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';

import { View } from './Themed';
import { AppStyles } from '../AppStyles';
import { CircularProgress } from '../components/CircularProgress';

export function ProgressIndicator({ progress = 0 }) {
    const theme = useTheme();
    const styles = React.useMemo(() => createStyles(theme), [theme]);

    const percent = progress * 33;
    const ringColor = progress === 1 ? 'red' : progress === 2 ? 'yellow' : 'green';

    return (
        <View style={styles.progressIcon}>
            <CircularProgress percent={percent} radius={8} bgRingWidth={4} progressRingWidth={3} ringColor={ringColor} ringBgColor={'#CCCCCC'} />
        </View>
    );
}
const createStyles = (theme: Theme) =>
    StyleSheet.create({
        progressIcon: {
            alignItems: 'center',
            justifyContent: 'center',
            marginRight: '1%',
            backgroundColor: AppStyles.color.white
        }
    });
