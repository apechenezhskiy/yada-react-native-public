import * as React from 'react';
import { StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { Theme, useTheme } from '@react-navigation/native';

import { View } from './Themed';
import { InputProps } from '../types';
import { VectorIcon } from './VectorIcon';
import { AppStyles } from '../AppStyles';

export function Input(props: InputProps) {
    const theme = useTheme();
    const styles = React.useMemo(() => createStyles(theme), [theme]);
    const { isValid, isLeftIcon, showClearButton, clearButtonPressed, wrapperStyle, ...otherProps } = props;

    const onClearButtonPressed = React.useCallback(() => {
        if (isClearButtonVisible) {
            this.textInput.focus();
            if (clearButtonPressed) {
                clearButtonPressed();
            }
        }
    }, [showClearButton, props.value]);

    const setTextInputRef = React.useCallback((input) => {
        this.textInput = input;
    }, []);

    const isClearButtonVisible = showClearButton && props.value != '';

    return (
        <View style={[styles.inputContainer, wrapperStyle, isValid ? styles.inputError : null]}>
            {isLeftIcon && (
                <View style={[styles.leftIconParent, props.leftIconStyle]}>
                    <VectorIcon size={24} name={props.leftIcon || 'search-outline'} color={props.leftIconColor || AppStyles.color.white} />
                </View>
            )}
            <TextInput ref={setTextInputRef} style={[styles.inputBody, isLeftIcon ? styles.inputBodySearch : null]} {...otherProps} />
            <View style={[{ backgroundColor: 'transparent', alignSelf: 'center' }, isClearButtonVisible ? styles.fullOpacity : styles.noOpacity]}>
                <TouchableOpacity
                    style={[styles.clearButtonParent, isLeftIcon ? styles.clearButtonParentSearch : null]}
                    onPress={onClearButtonPressed}
                >
                    <VectorIcon size={30} name={'close-outline'} color={AppStyles.color.white} />
                </TouchableOpacity>
            </View>
        </View>
    );
}
const createStyles = (theme: Theme) =>
    StyleSheet.create({
        inputContainer: {
            width: AppStyles.textInputWidth.main,
            marginTop: 10,
            borderStyle: 'solid',
            marginLeft: 25,
            marginRight: 25,
            borderColor: 'gray',
            borderRadius: 5,
            borderWidth: 1,
            flexDirection: 'row'
        },
        fullOpacity: {
            opacity: 100
        },
        noOpacity: {
            opacity: 0
        },
        inputBody: {
            height: 42,
            paddingLeft: 20,
            paddingRight: 20,
            color: AppStyles.color.text
        },
        inputBodySearch: {
            color: 'white',
            borderColor: 'blue',
            borderWidth: 10
        },
        inputError: {
            borderColor: AppStyles.color.danger
        },
        clearButtonParent: {
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            left: -20,
            height: 36,
            width: 36,
            borderRadius: 8
        },
        clearButtonParentSearch: {
            left: -30
        },
        leftIconParent: {
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: AppStyles.color.lightPrimary,
            color: 'white',
            left: 16
        }
    });
