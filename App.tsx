import React from 'react';
import type { ReactNode } from 'react';
import { StatusBar, Linking, SafeAreaView } from 'react-native';
import InAppBrowser from 'react-native-inappbrowser-reborn';

import { SafeAreaProvider } from 'react-native-safe-area-context';
import { RootSiblingParent } from 'react-native-root-siblings';

import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';

import Amplify from 'aws-amplify';
import awsmobile from './src/aws-exports.js';
import AmplitudeUtils from './services/amplitudeUtils';

async function urlOpener(url, redirectUrl) {
    await InAppBrowser.isAvailable();
    const { type, url: newUrl } = await InAppBrowser.openAuth(url, redirectUrl, {
        showTitle: false,
        enableUrlBarHiding: true,
        enableDefaultShare: false,
        ephemeralWebSession: false
    });
    if (type === 'success') {
        Linking.openURL(newUrl);
    }
}

Amplify.configure({
    ...awsmobile,
    oauth: {
        ...awsmobile.oauth,
        urlOpener
    }
});

AmplitudeUtils.initAmplitude();

const CustomStatusBar = () => <StatusBar translucent={true} barStyle={'light-content'} />;

const App: () => ReactNode = () => {
    const colorScheme = useColorScheme();

    return (
        <SafeAreaProvider>
            <RootSiblingParent>
                <Navigation colorScheme={colorScheme} />
                <CustomStatusBar />
            </RootSiblingParent>
        </SafeAreaProvider>
    );
};

export default App;
