module.exports = {
    env: {
        browser: true,
        es2021: true,
        node: true
    },
    extends: ['eslint:recommended', 'plugin:react/recommended', 'plugin:@typescript-eslint/recommended', 'plugin:prettier/recommended'],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaFeatures: {
            jsx: true
        },
        ecmaVersion: 2020,
        sourceType: 'module'
    },
    settings: {
        react: {
            version: 'detect' // Tells eslint-plugin-react to automatically detect the version of React to use
        }
    },
    plugins: ['react', '@typescript-eslint'],
    rules: {
        'arrow-parens': 'warn',
        'class-methods-use-this': 'off',
        'comma-dangle': ['error', 'never'],
        'css-modules/no-unused-class': 'off',
        'object-curly-newline': 'off',
        indent: ['error', 4, { SwitchCase: 1 }],
        quotes: ['error', 'single', { allowTemplateLiterals: true }],
        'jsx-quotes': ['error', 'prefer-single'],
        'jsx-a11y/no-noninteractive-tabindex': 'off',
        'jsx-a11y/label-has-associated-control': 'off',
        'jsx-a11y/click-events-have-key-events': 'off',
        'linebreak-style': 'off',
        'operator-linebreak': 'off',
        'no-console': [
            'error',
            {
                allow: ['error']
            }
        ],
        'no-unused-vars': 'warn',
        'no-param-reassign': [
            'error',
            {
                props: false
            }
        ],
        'no-plusplus': 'off',
        'max-len': [
            'warn',
            {
                code: 150,
                comments: 150,
                tabWidth: 4,
                ignoreUrls: true,
                ignoreRegExpLiterals: true
            }
        ],
        'max-classes-per-file': 'warn',
        'no-use-before-define': 'off',
        'no-mixed-operators': 'off',
        'prefer-destructuring': [
            'error',
            {
                object: true,
                array: false
            }
        ],
        'react/sort-comp': 'off',
        'react/jsx-wrap-multilines': 'warn',
        'react/jsx-indent': 'off',
        'react/jsx-indent-props': 'off',
        'react/jsx-boolean-value': ['error', 'always'],
        'react/jsx-filename-extension': [
            'off',
            {
                extensions: ['.js', '.jsx']
            }
        ],
        'react/jsx-curly-brace-presence': [
            'error',
            {
                props: 'always',
                children: 'ignore'
            }
        ],
        'react/jsx-no-bind': 'error',
        'react/jsx-props-no-spreading': 'off',
        'prettier/prettier': [
            'error',
            {
                endOfLine: 'auto'
            }
        ],
        'import/prefer-default-export': 'off',
        'no-underscore-dangle': ['error', { allow: ['__PayNowReloadData'] }],
        'react/prop-types': 0
    }
};
