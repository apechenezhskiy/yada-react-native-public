export function confirmationCodeValidator(code) {
    if (!code) return "Code can't be empty."
    if (code.length != 6) return 'Code must be 6 characters long.'
    return ''
  }