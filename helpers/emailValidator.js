export function emailValidator(email) {
    const re = /^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/;
    if (!email) return `Email can't be empty.`;
    if (!re.test(email)) return 'Ooops! We need a valid email address.';
    return '';
}
