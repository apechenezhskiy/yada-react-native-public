import Toast from 'react-native-root-toast';
import { AppStyles } from '../AppStyles';

export function showToaster(message) {
    Toast.show(message, {
        duration: Toast.durations.LONG,
        position: 100,
        backgroundColor: AppStyles.color.background,
        textColor: AppStyles.color.danger
    });
}
